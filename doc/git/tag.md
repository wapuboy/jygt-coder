在 Git 中，标签（tag）用于标记特定的提交，通常用于发布版本或重要的里程碑。以下是如何设置、推送、查看和删除标签的详细步骤：

### 一、创建标签

#### 1. 轻量标签

轻量标签是指向特定提交的指针，通常用于快速标记。

```bash
git tag <tag_name>
```

示例：

```bash
git tag v1.0
```

#### 2. 注释标签

注释标签包含更多信息，如标签者的姓名、邮箱、日期和消息，通常推荐使用此类型。

```bash
git tag -a <tag_name> -m "Tag message"
```

示例：

```bash
git tag -a v1.0 -m "Release version 1.0"
```

### 二、推送标签到远程仓库

默认情况下，标签不会在执行 `git push` 时被推送到远程仓库。需要显式推送标签：

#### 1. 推送特定标签

```bash
git push origin <tag_name>
```

示例：

```bash
git push origin v1.0
```

#### 2. 推送所有标签

```bash
git push --tags
```

### 三、查看标签

要查看仓库中的所有标签，可以使用：

```bash
git tag
```

要查看特定标签的详细信息（仅适用于注释标签）：

```bash
git show <tag_name>
```

示例：

```bash
git show v1.0
```

### 四、删除标签

#### 1. 删除本地标签

```bash
git tag -d <tag_name>
```

示例：

```bash
git tag -d v1.0
```

#### 2. 删除远程标签

要从远程仓库删除标签，可以使用：

```bash
git push --delete origin <tag_name>
```

示例：

```bash
git push --delete origin v1.0
```

### 五、拉取标签

如果您想从远程仓库拉取标签，可以使用以下命令：

```bash
git fetch --tags
```

要从远程 Git 仓库拉取指定标签的代码版本，可以按照以下步骤进行操作：
1. **拉取所有标签**：`git fetch --tags`
2. **检出指定标签**：`git checkout <tag_name>`
3. **（可选）从标签创建新分支**：`git checkout -b <new_branch_name> <tag_name>`

通过这些步骤，您可以成功地从远程 Git 仓库拉取并使用指定标签的代码版本。

#### 步骤 1：拉取所有标签

首先，您需要从远程仓库拉取所有标签，以确保您的本地仓库知道所有可用的标签。

```bash
git fetch --tags
```

#### 步骤 2：检出指定标签

拉取标签后，您可以检出想要的特定标签。这将使您处于“分离头指针”（detached HEAD）状态，意味着您不在任何分支上，而是在标签指向的特定提交上。

```bash
git checkout <tag_name>
```

例如，如果您想检出名为 `v1.0` 的标签，可以运行：

```bash
git checkout v1.0
```

#### 步骤 3：（可选）从标签创建分支

如果您想在该标签的基础上进行修改，最好从该标签创建一个新分支。您可以运行以下命令：

```bash
git checkout -b <new_branch_name> <tag_name>
```

例如：

```bash
git checkout -b my-feature-branch v1.0
```


### 总结

- 使用 `git tag <tag_name>` 创建轻量标签。
- 使用 `git tag -a <tag_name> -m "Tag message"` 创建注释标签。
- 使用 `git push origin <tag_name>` 推送特定标签，或使用 `git push --tags` 推送所有标签。
- 使用 `git tag` 查看标签，使用 `git show <tag_name>` 查看特定标签的详细信息。
- 使用 `git tag -d <tag_name>` 删除本地标签，使用 `git push --delete origin <tag_name>` 删除远程标签。
- 使用 `git fetch --tags` 拉取远程标签。

通过以上步骤，您可以有效地管理 Git 中的标签。