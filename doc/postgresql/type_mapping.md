在将 PostgreSQL 的字段类型映射到 SQLAlchemy 类型时，您可以使用以下常见的对应关系：

| PostgreSQL 数据类型      | SQLAlchemy 类型            |
|------------------------|-------------------------|
| `INTEGER`              | `Integer`               |
| `SERIAL`               | `Integer`               |
| `BIGINT`               | `BigInteger`            |
| `VARCHAR`              | `String`                |
| `TEXT`                 | `Text`                  |
| `BOOLEAN`              | `Boolean`               |
| `DATE`                 | `Date`                  |
| `TIMESTAMP`            | `DateTime`              |
| `FLOAT`                | `Float`                 |
| `DECIMAL`              | `Numeric`               |
