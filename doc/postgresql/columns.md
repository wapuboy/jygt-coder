要获取 PostgreSQL 数据库中表的结构信息和索引信息，并将表字段类型与 Graphene 的类型对应，可以按照以下步骤进行：

### 一、获取 PostgreSQL 表结构信息

您可以使用 SQLAlchemy 或直接通过 SQL 查询来获取表结构信息。

#### 1. 使用 SQLAlchemy 获取表结构信息

```python
from sqlalchemy import create_engine, MetaData

# 创建数据库连接
engine = create_engine('postgresql://username:password@localhost:5432/mydatabase')

# 创建 MetaData 对象
metadata = MetaData()

# 反射数据库中的所有表
metadata.reflect(bind=engine)

# 打印所有表及其列的信息
for table in metadata.sorted_tables:
    print(f"Table: {table.name}")
    for column in table.columns:
        print(f" - Column: {column.name}, Type: {column.type}, Nullable: {column.nullable}")
```

#### 2. 使用 SQL 查询获取表结构信息

您也可以直接使用 SQL 查询来获取指定表的列信息：

```sql
SELECT column_name, data_type, is_nullable
FROM information_schema.columns
WHERE table_name = 'your_table_name';
```

在 Python 中执行此查询的示例：

```python
import psycopg2

# 创建数据库连接
conn = psycopg2.connect("dbname=mydatabase user=username password=password host=localhost")
cur = conn.cursor()

# 执行查询
cur.execute("""
    SELECT column_name, data_type, is_nullable
    FROM information_schema.columns
    WHERE table_name = 'your_table_name';
""")

# 获取结果
columns = cur.fetchall()
for column in columns:
    print(f"Column: {column[0]}, Type: {column[1]}, Nullable: {column[2]}")

# 关闭连接
cur.close()
conn.close()
```

### 二、获取 PostgreSQL 索引信息

要获取表的索引信息，您可以使用以下 SQL 查询：

```sql
SELECT indexname, indexdef
FROM pg_indexes
WHERE tablename = 'your_table_name';
```

在 Python 中执行此查询的示例：

```python
cur.execute("""
    SELECT indexname, indexdef
    FROM pg_indexes
    WHERE tablename = 'your_table_name';
""")

# 获取结果
indexes = cur.fetchall()
for index in indexes:
    print(f"Index Name: {index[0]}, Definition: {index[1]}")
```

### 三、表字段类型与 Graphene 类型的对应

在将 PostgreSQL 的字段类型映射到 Graphene 类型时，您可以使用以下常见的对应关系：

| PostgreSQL 数据类型      | Graphene 类型            |
|------------------------|-------------------------|
| `INTEGER`              | `graphene.Int`          |
| `SERIAL`               | `graphene.Int`          |
| `BIGINT`               | `graphene.Int`          |
| `VARCHAR`              | `graphene.String`       |
| `TEXT`                 | `graphene.String`       |
| `BOOLEAN`              | `graphene.Boolean`      |
| `DATE`                 | `graphene.Date`         |
| `TIMESTAMP`            | `graphene.DateTime`     |
| `FLOAT`                | `graphene.Float`        |
| `DECIMAL`              | `graphene.Float`        |

### 示例：映射 PostgreSQL 表到 Graphene

假设您有一个 PostgreSQL 表 `User`，可以定义相应的 SQLAlchemy 模型和 Graphene 类型：

```python
from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)
    is_active = Column(Boolean)

class UserType(SQLAlchemyObjectType):
    class Meta:
        model = User

class Query(graphene.ObjectType):
    users = graphene.List(UserType)

    def resolve_users(self, info):
        query = UserType.get_query(info)
        return query.all()

schema = graphene.Schema(query=Query)
```

### 总结

1. **获取表结构信息**：使用 SQLAlchemy 或 SQL 查询从 `information_schema.columns` 获取列信息。
2. **获取索引信息**：使用 SQL 查询从 `pg_indexes` 获取索引信息。
3. **字段类型对应**：根据 PostgreSQL 数据类型与 Graphene 类型的对应关系进行映射。