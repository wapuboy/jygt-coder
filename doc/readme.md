# 使用WebAPI工程

## 1. getAll{table_name}
查询表的所有记录，提供分页
![alt text](image/webapi/getAll.png)
```graphql
query{
  getAllTUser(page: 0, number: 2) {
    currentPage
    total
    pages
    items {
      fId
      fUid
      fName
      fMotto
      fAuthCode
      fAvartaUrl
      fCreateTime
      fModifyTime
      fUserSourceId
    }
  }
}

```

## 2. getBy{index_name}Of{table_name}
- 主键查询，JYGT-CODER默认所有表都有主键，与唯一索引查询一样
![alt text](image/webapi/getByPrimary.png)
```graphql
query{
  getByPrimaryOfTUser(paraFId: 12) {
    fId
    fUid
    fName
    fMotto
    fAuthCode
    fAvartaUrl
    fCreateTime
    fModifyTime
    fUserSourceId
  }
}
```

- 按照索引名查询，唯一索引将直接返回对象。
![alt text](image/webapi/getByIndexName_unq.png)
```graphql
query{
  getByUnqSourceAndUidOfTUser(
    paraFUserSourceId:10,
    paraFUid:"test_f_uidryhvImOADa"
  ){
    fId
    fUid
    fName
    fMotto
    fAuthCode
    fAvartaUrl
    fCreateTime
    fModifyTime
    fUserSourceId
  }
}
```

- 非唯一索引将返回一个数组
![alt text](image/webapi/getBuIndexName_idx.png)
```graphql
query{
  getByIdxSourceOfTUser(paraFUserSourceId: 10) {
    fId
    fUid
    fName
    fMotto
    fAuthCode
    fAvartaUrl
    fCreateTime
    fModifyTime
    fUserSourceId
  }
}
```

## 3. add{table_name}
![add](image/webapi/add.png)
```graphql
mutation{
  addTUser(
    inTUserGqlObjects: [
      {
        fId: 101234, 
        fUid: "test_f_uidryhvImOADa_104", 
        fName: "test_f_nameDdLDALNScf", 
        fMotto: "test_f_mottomfhfFToPjf", 
        fAuthCode: "test_f_auth_codeLmVHWQfeVK", 
        fAvartaUrl: "test_f_avarta_urliFrsLFuLXA", 
        fCreateTime: "2024-03-10T00:00:00", 
        fModifyTime: "2021-08-08T00:00:00", 
        fUserSourceId: 10
      }
    ]
  ) {
    res {
      fId
      fUid
      fName
      fMotto
      fAuthCode
      fAvartaUrl
      fCreateTime
      fModifyTime
      fUserSourceId
    }
  }
}
```

## 4. update{table_name}
![update](image/webapi/update.png)
```graphql
mutation{
  updateTUser(
    inTUserGqlObjects: [
      {
        fId: 2012, 
        fUid: "test_f_uidryhvImOADa_2020", 
        fName: "test_f_nameDdLDALNScf_10", 
        fMotto: "test_f_mottomfhfFToPjf", 
        fAuthCode: "test_f_auth_codeLmVHWQfeVK", 
        fAvartaUrl: "test_f_avarta_urliFrsLFuLXA", 
        fCreateTime: "2024-03-10T00:00:00", 
        fModifyTime: "2021-08-08T00:00:00", 
        fUserSourceId: 10
      }
    ]
  ) {
    res {
      fId
      fUid
      fName
      fMotto
      fAuthCode
      fAvartaUrl
      fCreateTime
      fModifyTime
      fUserSourceId
    }
  }
}
```

## 5. delete{table_name}
![delete](image/webapi/delete.png)
```graphql
mutation{
  deleteTUser(
    fIds:[
      1,
      26,
      24
    ]
  ) {
    res {
      fId
      fUid
      fName
      fMotto
      fAuthCode
      fAvartaUrl
      fCreateTime
      fModifyTime
      fUserSourceId
    }
  }
}
```

## 6. replace{table_name}
![replace](image/webapi/replace.png)

```graphql
mutation{
  replaceTUser(
    inTUserGqlObjects: [
      {
        fId: 1012, 
        fUid: "test_f_uidryhvImOADa", 
        fName: "test_f_nameDdLDALNScf_10", 
        fMotto: "test_f_mottomfhfFToPjf", 
        fAuthCode: "test_f_auth_codeLmVHWQfeVK", 
        fAvartaUrl: "test_f_avarta_urliFrsLFuLXA", 
        fCreateTime: "2024-03-10T00:00:00", 
        fModifyTime: "2021-08-08T00:00:00", 
        fUserSourceId: 10
      },
      {
        fId: 5512, 
        fUid: "test_f_uidryhvImOADa——55", 
        fName: "test_f_nameDdLDALNScf_10", 
        fMotto: "test_f_mottomfhfFToPjf", 
        fAuthCode: "test_f_auth_codeLmVHWQfeVK", 
        fAvartaUrl: "test_f_avarta_urliFrsLFuLXA", 
        fCreateTime: "2024-03-10T00:00:00", 
        fModifyTime: "2021-08-08T00:00:00", 
        fUserSourceId: 10
      }
    ]
  ) {
    res {
      fId
      fUid
      fName
      fMotto
      fAuthCode
      fAvartaUrl
      fCreateTime
      fModifyTime
      fUserSourceId
      
    }
  }
}
```