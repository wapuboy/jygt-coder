在 MySQL 中，`SHOW FULL COLUMNS FROM t_user_relation` 命令用于显示指定表（在此例中为 `t_user_relation`）的列信息。输出的信息包括多个字段，每个字段都有其特定的含义。以下是一些常见字段的解释，包括 `Key` 字段的含义：

### 常见字段含义

1. **Field**: 列的名称。
2. **Type**: 列的数据类型，例如 `VARCHAR(255)`、`INT`、`DATETIME` 等。
3. **Collation**: 列的字符集和排序规则（仅适用于字符类型）。
4. **Null**: 指示该列是否可以为 `NULL`。通常显示为 `YES` 或 `NO`。
5. **Key**: 指示该列是否是索引的一部分。可能的值包括：
   - **PRI**: 表示该列是主键（Primary Key）。主键是唯一标识表中每一行的列，不能有重复值。
   - **UNI**: 表示该列是唯一索引（Unique Key）。唯一索引确保该列中的所有值都是唯一的，但允许有一个 `NULL` 值（如果该列允许 `NULL`）。
   - **MUL**: 表示该列是普通索引（Multiple Key）。这意味着该列可以包含重复值，且可以用于加速查询。
6. **Default**: 列的默认值。如果没有指定默认值，则显示为 `NULL`。
7. **Extra**: 额外的信息，例如 `auto_increment`（自增）等。

### 示例

假设您在执行 `SHOW FULL COLUMNS FROM t_user_relation` 后得到如下结果：

| Field            | Type          | Collation      | Null | Key | Default | Extra          |
|------------------|---------------|----------------|------|-----|---------|-----------------|
| id               | int(11)      | NULL           | NO   | PRI | NULL    | auto_increment   |
| user_id          | int(11)      | NULL           | NO   | MUL | NULL    |                 |
| relation_type    | varchar(50)  | utf8_general_ci| YES  |     | NULL    |                 |
| created_at       | datetime      | NULL           | NO   |     | NULL    |                 |

在这个示例中：
- `id` 列是主键（`PRI`），并且是自增的。
- `user_id` 列是一个普通索引（`MUL`），允许重复值。
- `relation_type` 列没有索引。
- `created_at` 列也没有索引。

### 总结

`SHOW FULL COLUMNS` 命令提供了有关表中列的详细信息，其中 `Key` 字段的值可以帮助您了解每个列在索引中的角色。理解这些字段的含义对于数据库设计和优化查询性能非 常重要。