# MySQL、SQLAlchemy、Graphene类型映射

+ [MySQL到GraphQL类型映射](https://www.sqlalchemy.org/docs/core/type_basics.html)
    - [MySQL到GraphQL类型映射](#MySQ-GraphQL)
+ [GraphQL类型映射](https://graphql.org/learn/schema/)
    - [MySQLSQLAlchemy类型映射](#MySQL-SQLAlchemy)

## MySQL GraphQL
当使用GraphQL和MySQL数据库时，通常需要将MySQL数据类型映射到GraphQL类型。以下是常见MySQL数据类型与其对应的GraphQL类型的映射。

### MySQL到GraphQL类型映射

| MySQL数据类型           | GraphQL类型                  | 描述                                             |
|------------------------|-----------------------------|-------------------------------------------------|
| `VARCHAR`, `CHAR`      | `graphene.String`           | 表示可变长度或固定长度的字符串。                 |
| `TEXT`                 | `graphene.String`           | 表示长文本字符串。                               |
| `INT`, `TINYINT`       | `graphene.Int`              | 表示整数值。                                    |
| `BIGINT`               | `graphene.Int`              | 如果超出`Int`的限制，也可以表示为`graphene.Float`。 |
| `FLOAT`, `DOUBLE`      | `graphene.Float`            | 表示浮点数。                                    |
| `DECIMAL`              | `graphene.Float`            | 表示定点数。                                    |
| `DATE`                 | `graphene.String`           | 通常表示为“YYYY-MM-DD”格式的字符串。            |
| `DATETIME`, `TIMESTAMP`| `graphene.String`           | 通常表示为“YYYY-MM-DD HH:MM:SS”格式的字符串。  |
| `TIME`                 | `graphene.String`           | 通常表示为“HH:MM:SS”格式的字符串。              |
| `BOOLEAN`              | `graphene.Boolean`          | 表示布尔值（真/假）。                           |
| `BLOB`                 | `graphene.String`           | 可以表示为字符串（base64编码）或自定义类型。   |

### 在Graphene中的示例映射

以下是如何在`graphene`中定义一个与MySQL表结构对应的GraphQL类型的示例：

```python
import graphene

# 示例MySQL表结构
# CREATE TABLE User (
#     id INT PRIMARY KEY AUTO_INCREMENT,
#     username VARCHAR(50),
#     email VARCHAR(100),
#     age INT,
#     created_at DATETIME
# );

class UserType(graphene.ObjectType):
    id = graphene.Int()
    username = graphene.String()
    email = graphene.String()
    age = graphene.Int()
    created_at = graphene.String()  # 你也可以使用自定义标量来处理日期/时间

# 示例查询
class Query(graphene.ObjectType):
    user = graphene.Field(UserType, id=graphene.Int())

    def resolve_user(self, info, id):
        # 在这里你通常会查询MySQL数据库以获取用户
        # 例如：
        # return get_user_from_db(id)
        pass

schema = graphene.Schema(query=Query)
```

### 注意事项

1. **日期和时间类型**：虽然MySQL有特定的日期和时间类型，在GraphQL中，通常将它们表示为字符串。你可以考虑为日期和时间定义自定义标量以更好地处理。

2. **自定义标量**：如果你想要强制执行特定格式的日期或其他类型，可以在Graphene中定义自定义标量。

3. **错误处理**：在解析字段时，确保处理数据可能不存在或类型不匹配的情况。

4. **数据库ORM**：如果你使用像SQLAlchemy或Django ORM这样的ORM，它们通常提供内置支持来将数据库类型映射到Python类型，从而简化该过程。

通过遵循此映射，你可以有效地创建与MySQL数据库结构对齐的GraphQL架构。如果你有特定的类型或用例想要进一步讨论，请随时提问！

## MySQL SQLAlchemy

在将MySQL数据类型映射到SQLAlchemy类型时，可以使用以下表格作为参考。SQLAlchemy提供了多种类型，这些类型与MySQL数据类型相对应，使您能够在Python中定义数据库模式。

### MySQL到SQLAlchemy类型映射

| MySQL数据类型           | SQLAlchemy类型               | 描述                                             |
|------------------------|-----------------------------|-------------------------------------------------|
| `VARCHAR`, `CHAR`      | `String(length)`            | 表示可变长度或固定长度的字符串。                 |
| `TEXT`                 | `Text`                      | 表示长文本字符串。                               |
| `INT`, `TINYINT`       | `Integer`                   | 表示整数值。                                    |
| `BIGINT`               | `BigInteger`                | 表示大整数值。                                  |
| `FLOAT`, `DOUBLE`      | `Float`                     | 表示浮点数。                                    |
| `DECIMAL`              | `Numeric(precision, scale)` | 表示定点数。                                    |
| `DATE`                 | `Date`                      | 表示日期（YYYY-MM-DD）。                        |
| `DATETIME`, `TIMESTAMP`| `DateTime`                  | 表示日期和时间（YYYY-MM-DD HH:MM:SS）。        |
| `TIME`                 | `Time`                      | 表示时间（HH:MM:SS）。                          |
| `BOOLEAN`              | `Boolean`                   | 表示布尔值（真/假）。                           |
| `BLOB`                 | `LargeBinary`               | 表示二进制数据。                                |

### 在SQLAlchemy中的示例映射

以下是如何定义一个与MySQL表结构对应的SQLAlchemy模型的示例：

```python
from sqlalchemy import create_engine, Column, Integer, String, DateTime, Boolean, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

# 示例MySQL表结构
# CREATE TABLE User (
#     id INT PRIMARY KEY AUTO_INCREMENT,
#     username VARCHAR(50),
#     email VARCHAR(100),
#     age INT,
#     created_at DATETIME
# );

class User(Base):
    __tablename__ = 'user'
    
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(50), nullable=False)
    email = Column(String(100), nullable=False)
    age = Column(Integer)
    created_at = Column(DateTime)

# 示例：设置数据库
engine = create_engine('mysql+pymysql://username:password@localhost:3306/mydatabase')
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

# 示例：添加新用户
new_user = User(username='john_doe', email='john@example.com', age=30, created_at=datetime.now())
session.add(new_user)
session.commit()
```

### 注意事项

1. **数值类型**：对于`DECIMAL`，您可以在`Numeric`类型中指定精度和小数位数（例如，`Numeric(10, 2)`表示最多10位数字，其中2位可以在小数点后）。

2. **日期和时间类型**：SQLAlchemy提供`Date`、`DateTime`和`Time`类型，以适当地处理MySQL的日期和时间字段。

3. **布尔类型**：SQLAlchemy中的`Boolean`类型直接映射到MySQL的`BOOLEAN`类型。

4. **二进制数据**：对于二进制大对象（BLOB），使用`LargeBinary`。

5. **创建数据库**：`create_all`方法根据定义的模型在数据库中创建表。

通过使用此映射，您可以有效地定义SQLAlchemy模型，以与MySQL数据库架构相对应。如果您有特定的类型或用例想要进一步讨论，请随时提问！