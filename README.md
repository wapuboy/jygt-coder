# JYGT-CODER 

#### 介绍
JYGT-CODER （晶莹果甜代码生成工具） 是一个自动化代码生成工具，用来基于数据库结构生成GraphQL风格的WebAPI服务。WebAPI服务工程的代码可用来二次开发，以加速应用开发速度。相关需求和设计，参考如下资料：
- [需求与调研](https://www.toutiao.com/article/7436216201740648995/)
- [设计与验证](https://www.toutiao.com/article/7442521216507871771/)

#### 软件架构
JYGT-CODER 从数据库获取表的字段和索引信息，并基于这些信息生成一个提供GraphQL风格的WebAPI服务。如下图所示：
![系统组成](doc/image/aa-top.jpeg)
- Database ： 一个数据库，如MySQL、PostgreSQL等。CODER将从这里读取表Tables的字段Field和索引Index信息。
- WebAPI ：一个由CODER生成的工程，包含所有源代码和描述文件。运行后，会提供一个GraphQL风格的WebAPI服务。支持对数据库表的CRUD操作，包含如下常用API：
    1. getAll{table_name} : 获取指定表的所有数据，支持分页
    2. getBy{Index_name}Of{table_name}  ： 按照表的索引获取数据，获取指定表的单个()或多行数据
    3. delete{table_name} ：删除指定主键的数据，主键是一个集合
    4. add{table_name} ：增加一组数据，当数据存在则丢弃
    5. update{table_name} ：修改一组数据，当数据不存在则丢弃
    6. replace{table_name} ：替换一组数据，当数据存在时更新，当数据不存在时增加
- CODER : JYGT-CODER。 提供命令行工具(CommandLine)，用来支持用户在命令行窗口下基于Database生成WebAPI；浏览器的向导(Web Page)，用来支持用户通过页面来生成WebAPI；定时任务(Auto Task)，用于异步执行任务，并及时检测Database的变化并刷新WebAPI。
#### 安装教程
下载本工具后，进入工具根目录，其下目录结构如下：
```bash
├─log       # 运行日志 ：  需要定时清空
├─bin       # 执行脚本
├─dat       # 输入输出数据 ： 生成的WEB API代码放在这里    
├─src       # 程序代码
├─tmp       # 临时目录：   可清空
└─doc       # 相关文档
```

进入本工具根目录，执行如下命令，获得命令行帮助
```bash
python src/entry/cmdline.py -h
usage: cmdline.py [-h] [-i DB_HOST] [-p DB_PORT] [-u DB_USER] [-s DB_PASSWORD] [-d DB_DATABASE] [-I API_HOST] [-P API_PORT] [-U API_PATH] [-r TAR_ROOT_DIR]
                  [-v TAR_VER]

JYGT-CODER 命令行工具，用于获取任务描述，并读取数据库表、字段和索引信息.

options:
  -h, --help            show this help message and exit
  -i, --db_host DB_HOST
                        数据库地址
  -p, --db_port DB_PORT
                        数据库端口
  -u, --db_user DB_USER
                        数据库用户名
  -s, --db_password DB_PASSWORD
                        数据库密码
  -d, --db_database DB_DATABASE
                        数据库名称
  -I, --api_host API_HOST
                        生成的WebAPI运行所在的地址
  -P, --api_port API_PORT
                        生成的WebAPI运行所在的端口
  -U, --api_path API_PATH
                        生成的WebAPI的URL根路径
  -r, --tar_root_dir TAR_ROOT_DIR
                        目标代码存放根路径，默认为项目根目录下dat/dist/目录
  -v, --tar_ver TAR_VER
                        目标代码版本号，默认为1.0.0

```
- 本示例提供一个MySQL的数据库脚本[db_jygt.sql](src/db/mysql/db_jygt.sql),可以用来创建如下的数据库表。![alt text](doc/image/db_jygt.png)
- 默认--api_path 为 /api，可通过命令行参数修改。  必须为/开头的字符串
- 本工程默认提供graphiql，路径为$(api_path)_graphiql
#### 使用说明

生成的WebAPI工程，默认放在dat/dist/$database_name/目录下。进入这个目录后，可以按如下步骤使用此代码：
1. 工程代码
```
├─bin   # 执行脚本
├─dat   # 输入输出数据
├─doc   # 相关文档
├─log   # 运行日志
├─src   # 程序代码
└─tmp   # 临时目录
```
WebAPI工程的src子目录下目录结构：
```
├─connection    # 数据库连接配置
├─entity        # 数据库表的实体类
├─graphql       # Graphql的对象、查询和修改方法，以及模式定义文件
│  ├─mutation   # 修改方法
│  ├─query      # 查询方法
│  ├─schema     # 模式定义文件
│  └─type       # 类型定义
├─public        # 公共库文件
├─service       # 数据库的服务类
└─test          # 测试用例
```

2. 测试报告
进入WebAPI工程根目录，执行如下命令：
```bash
pytest --html=./html/report.html
```
浏览器打开生成的./html/report.html，可看到相应的测试报告
![alt text](doc/image/test_report.png)

3. 启动服务
```bash
# python .\dat\dist\db_jygt\src\main.py                
```
4. 访问服务
按照api_path的设置，确认访问的url路径，默认为：
- webAPI接口 ： http://localhost:5000/api
- graphiql调试页面 ：http://localhost:5000/api_graphiql
- [查看示例](doc/readme.md)

#### 参与贡献
如果对此项目感兴趣，无论是希望加入一起开发，或者赞助和使用本工具，欢迎联系我。
- Email ：7457222@qq.com
- 公众号 ：修炼者
![修炼者](doc/image/wechat-s-w.png)


