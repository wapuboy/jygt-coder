@echo off
:: 更改终端字符集为 UTF-8
chcp 65001
:: 获取批处理文件所在的目录（bin目录）
set BIN_DIR=%~dp0

:: 获取项目的根目录（bin的父目录）
set PROJECT_ROOT=%BIN_DIR%..\

:: 将路径转换为绝对路径，移除结尾的反斜杠
for %%i in ("%PROJECT_ROOT%") do set PROJECT_ROOT=%%~fi

echo %PROJECT_ROOT%

:: 获取src目录的完整路径
set SRC_DIR=%PROJECT_ROOT%src

:: 检查 Python 是否已安装
where python >nul 2>&1
if %errorlevel% neq 0 (
    echo Python 未安装。请先安装 Python。
    exit /b 1
)

:: 创建虚拟环境
python -m venv .venv

:: 激活虚拟环境
call .venv\Scripts\activate

echo 正在安装依赖包...
pip install -r %SRC_DIR%\requirements.txt
if %errorlevel% neq 0 (
    echo 安装依赖包失败，请检查 requirements.txt 文件。
    exit /b 1
)

:: 检查 PYTHONPATH 是否已经设置，如果未设置则初始化为 src 目录
if "%PYTHONPATH%"=="" (
    set PYTHONPATH=%SRC_DIR%
) else (
    REM 使用 findstr 检查 PYTHONPATH 是否包含 SRC_DIR
    echo %PYTHONPATH% | findstr /c:"%SRC_DIR%" >nul
    if errorlevel 1 (
        REM 如果未找到 SRC_DIR ，则将其添加到 PYTHONPATH 末尾
        set "PYTHONPATH=%PYTHONPATH%;%SRC_DIR%"
    ) else (
        REM 如果已找到 SRC_DIR ，输出提示信息（可选）
        echo PYTHONPATH 已经包含: %SRC_DIR%
    )
)

:: 打印出新的 PYTHONPATH 以确认设置
echo PYTHONPATH 当前是: %PYTHONPATH%

:: 调用Python解释器并运行入口文件
if "%~1"=="" (
    REM 如果没有参数，则执行 python %SRC_DIR%\entry\cmdline.py -h
    python %SRC_DIR%\entry\cmdline.py -h
) else (
    REM 如果有参数，则将所有参数替换为 -h
    REM python %SRC_DIR%\entry\cmdline.py -h
    REM 或者如果你想保留原始参数以便后续处理，可以这样写：
    python %SRC_DIR%\entry\cmdline.py %*
)

:: 结束批处理文件时恢复环境变量
endlocal