#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import os
import sys
import random

from datetime import datetime, timedelta

start_date = datetime(2020, 1, 1)
end_date = datetime(2024, 12, 31)

def generate_random_date():
    delta = end_date - start_date
    random_days = random.randint(0, delta.days)
    random_date = start_date + timedelta(days=random_days)
    return random_date


# 添加当前文件所在目录到Python路径中
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

# 添加包的父目录到Python路径中
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)


import pytest

from service.TMenuService import TMenuService
from entity.TMenuEntity import TMenuEntity


@pytest.fixture
def getObj():
    return TMenuService()

columns = [{'name': 'f_id', 'type': 'Integer', 'gql_type': 'graphene.Int', 'gql_required': 'required=False', 'nullable': 'nullable=False', 'default': 'default=None', 'comment': "'auto_increment'", 'primary_key': 'primary_key=True,'}, {'name': 'f_name', 'type': 'String(64)', 'gql_type': 'graphene.String', 'gql_required': 'required=False', 'nullable': 'nullable=False', 'default': 'default=None', 'comment': "''", 'primary_key': ''}, {'name': 'f_parent_id', 'type': 'Integer', 'gql_type': 'graphene.Int', 'gql_required': 'required=True', 'nullable': 'nullable=True', 'default': 'default=None', 'comment': "''", 'primary_key': ''}, {'name': 'f_label', 'type': 'String(64)', 'gql_type': 'graphene.String', 'gql_required': 'required=True', 'nullable': 'nullable=True', 'default': 'default=None', 'comment': "''", 'primary_key': ''}, {'name': 'f_icon', 'type': 'String(128)', 'gql_type': 'graphene.String', 'gql_required': 'required=True', 'nullable': 'nullable=True', 'default': 'default=None', 'comment': "''", 'primary_key': ''}, {'name': 'f_url', 'type': 'String(128)', 'gql_type': 'graphene.String', 'gql_required': 'required=True', 'nullable': 'nullable=True', 'default': 'default=None', 'comment': "''", 'primary_key': ''}, {'name': 'f_paras', 'type': 'String(512)', 'gql_type': 'graphene.String', 'gql_required': 'required=True', 'nullable': 'nullable=True', 'default': 'default=None', 'comment': "''", 'primary_key': ''}, {'name': 'f_desc', 'type': 'String(512)', 'gql_type': 'graphene.String', 'gql_required': 'required=True', 'nullable': 'nullable=True', 'default': 'default=None', 'comment': "''", 'primary_key': ''}, {'name': 'f_state', 'type': 'Integer', 'gql_type': 'graphene.Int', 'gql_required': 'required=False', 'nullable': 'nullable=False', 'default': "default='0'", 'comment': "''", 'primary_key': ''}, {'name': 'f_create_time', 'type': 'DateTime', 'gql_type': 'graphene.String', 'gql_required': 'required=False', 'nullable': 'nullable=False', 'default': 'default=func.now', 'comment': "''", 'primary_key': ''}, {'name': 'f_modify_time', 'type': 'DateTime', 'gql_type': 'graphene.String', 'gql_required': 'required=True', 'nullable': 'nullable=True', 'default': 'default=func.now', 'comment': "'on update CURRENT_TIMESTAMP'", 'primary_key': ''}]

import string

def generate_random_string(length):
    letters = string.ascii_letters  # Includes both uppercase and lowercase letters
    random_string = ''.join(random.choice(letters) for _ in range(length))
    return random_string

def generate_test_data(columns):
    res = []
    while len(res) < 10:
        test_data = {}
        for column in columns:
            col_name = column['name']
            col_type = column['type']
            if col_type == 'Integer':
                test_data[col_name] = random.randint(1, 100)
            elif col_type.startswith('String'):
                test_data[col_name] = f'test_{col_name}{generate_random_string(10)}'
            elif col_type == 'Text':
                test_data[col_name] = f'test_{col_name}{generate_random_string(10)}'
            elif col_type == 'DateTime':
                test_data[col_name] = generate_random_date()
            elif col_type == 'Boolean':
                test_data[col_name] = True
            else:
                test_data[col_name] = None
        res.append(test_data)
    return res

def generate_test_data_primary(columns):
    res = []
    while len(res) < 10:
        test_data = ''
        for column in columns:
            col_name = column['name']
            col_type = column['type']
            col_primary_key = column['primary_key']

            if col_primary_key.startswith('primary_key'):
                if col_type == 'Integer':
                    test_data = random.randint(1, 100)
                elif col_type.startswith('String'):
                    test_data = f'test_{col_name}{generate_random_string(10)}'
                elif col_type == 'Text':
                    test_data = f'test_{col_name}{generate_random_string(10)}'
                elif col_type == 'DateTime':
                    test_data = generate_random_date()
                elif col_type == 'Boolean':
                    test_data = True
                else:
                    test_data = None
        res.append(test_data)
    return res

def test_getAll(getObj):

    res = getObj.getAll()

    if res['total'] == 0:
        return
    
    print(f"total: {res['total']},pages: {res['pages']},current_page: {res['current_page']}")

    for itm in res["items"]:
        print(itm)
        assert isinstance(itm, TMenuEntity)

def test_add_and_get_by(getObj):
    """
    测试 add 方法和 getBy 方法。
    """
    test_data = generate_test_data(columns)

    # 添加数据
    test_data_ed =  getObj.add(test_data)

    if len(test_data_ed) == 0:
        return

    # 获取数据
    primary_key = getattr(test_data_ed[0],'f_id')
    retrieved_data = getObj.getByPrimary(primary_key)
   
    assert retrieved_data is not None

    for column in columns:
        col_name = column['name']
        assert getattr(retrieved_data, col_name) == getattr(test_data_ed[0], col_name)
    

    
def test_update_and_get_by(getObj):
    """
    测试 add 方法和 getBy 方法。
    """
    test_data = generate_test_data(columns)

    # 添加数据
    test_data_ed =  getObj.update(test_data)

    if len(test_data_ed) == 0:
        return

    # 获取数据
    primary_key = getattr(test_data_ed[0],'f_id')
    retrieved_data = getObj.getByPrimary(primary_key)
   
    assert retrieved_data is not None

    for column in columns:
        col_name = column['name']
        assert getattr(retrieved_data, col_name) == getattr(test_data_ed[0], col_name)
    

    


def test_replace_and_get_by(getObj):
    """
    测试 add 方法和 getBy 方法。
    """
    test_data = generate_test_data(columns)

    # 添加数据
    test_data_ed =  getObj.replace(test_data)

    assert len(test_data_ed) == len(test_data)

    # 获取数据
    primary_key = getattr(test_data_ed[0],'f_id')
    retrieved_data = getObj.getByPrimary(primary_key)
   
    assert retrieved_data is not None
    
    for column in columns:
        col_name = column['name']
        assert getattr(retrieved_data, col_name) == getattr(test_data_ed[0], col_name)
    


def test_delete(getObj):
    """
    测试 delete 方法。
    """
    test_data = generate_test_data_primary(columns)

    # 添加数据
    test_data_ed = getObj.delete(test_data)

    for itm in test_data_ed:
        primary_key = getattr(test_data_ed[0],'f_id')
        # 获取删除后的数据
        retrieved_data = getObj.getByPrimary(primary_key)
        assert retrieved_data is None

    

        