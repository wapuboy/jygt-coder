#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
from sqlalchemy import Column, Integer, String, DateTime,Float,Text,BigInteger,Numeric,Date,Time,Boolean,LargeBinary,func
from sqlalchemy.orm import declarative_base
from datetime import datetime

Base = declarative_base()

class TUserEntity(Base):
    __tablename__ = 't_user'
    f_id = Column(Integer, primary_key=True, nullable=False)
    f_uid = Column(String(64),  nullable=False)
    f_user_source_id = Column(Integer,  nullable=False)
    f_auth_code = Column(String(64),  nullable=False)
    f_name = Column(String(64),  nullable=True)
    f_motto = Column(String(128),  nullable=True)
    f_avarta_url = Column(String(50),  nullable=True)
    f_create_time = Column(DateTime,  nullable=False,  default=func.now)
    f_modify_time = Column(DateTime,  nullable=True)

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def __repr__(self):
        attrs = ', '.join(f"{key}={value}" for key, value in vars(self).items() if not key.startswith('_'))
        return f"TUserEntity({attrs})"
