#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
from sqlalchemy import Column, Integer, String, DateTime,Float,Text,BigInteger,Numeric,Date,Time,Boolean,LargeBinary,func
from sqlalchemy.orm import declarative_base
from datetime import datetime

Base = declarative_base()

class TUserRoleEntity(Base):
    __tablename__ = 't_user_role'
    f_id = Column(Integer, primary_key=True, nullable=False)
    f_user_id = Column(Integer,  nullable=False)
    f_role_id = Column(Integer,  nullable=False,  default='0')
    f_desc = Column(String(128),  nullable=True)
    f_state = Column(Integer,  nullable=False,  default='0')
    f_create_time = Column(DateTime,  nullable=False,  default=func.now)
    f_modify_time = Column(DateTime,  nullable=False,  default=func.now)

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def __repr__(self):
        attrs = ', '.join(f"{key}={value}" for key, value in vars(self).items() if not key.startswith('_'))
        return f"TUserRoleEntity({attrs})"
