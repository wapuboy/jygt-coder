#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
"""
JYGT_CODER 主入口代码

作者：修炼者 (7457222@qq.com)
日期：2024-12-10
说明：
    执行此文件后，将启动Web服务，引导用户完成代码生成。
"""
from flask import Flask, render_template
from flask_graphql import GraphQLView

import sys,os
# 添加当前文件所在目录到Python路径中
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

# 添加包的父目录到Python路径中
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

app = Flask(__name__)

from mygraphql.schema.dbJygtSchema  import dbJygtSchema

app.add_url_rule('/api',
                 view_func=GraphQLView.as_view(
                    'dbJygtSchema',
                    schema=dbJygtSchema,
                    graphiql=False
                    ))

@app.route('/api_graphiql')
def graphiql():
    return render_template('custom_graphiql.html')  # No need to specify the 'templates' directory

@app.route('/')
def demo():
    return graphiql()

if __name__ == '__main__':
    app.run(debug=True,port=5000,host='localhost')

