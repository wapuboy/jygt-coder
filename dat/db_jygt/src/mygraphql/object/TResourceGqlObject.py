#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
from graphene_sqlalchemy import SQLAlchemyObjectType
import graphene

from entity.TResourceEntity import TResourceEntity

class TResourceGqlObject(SQLAlchemyObjectType):
    class Meta:
        model = TResourceEntity
    
    # 主键本是Int，graphql会将其转化为字符串，这里强制改为int
    f_id = graphene.Int()
class PaginatedTResourceGqlObject(graphene.ObjectType):
    items = graphene.List(TResourceGqlObject)
    total = graphene.Int()
    pages = graphene.Int()
    current_page = graphene.Int()

class TResourceGqlInputObject(graphene.InputObjectType):
   
    f_id = graphene.Int(required=False)
    f_resource_id = graphene.Int(required=False)
    f_resource_type = graphene.Int(required=False)
    f_name = graphene.String(required=False)
    f_desc = graphene.String(required=True)
    f_state = graphene.Int(required=False)
    f_create_time = graphene.String(required=False)
    f_modify_time = graphene.String(required=True)

