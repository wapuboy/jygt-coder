#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
from graphene_sqlalchemy import SQLAlchemyObjectType
import graphene

from entity.TFieldEntity import TFieldEntity

class TFieldGqlObject(SQLAlchemyObjectType):
    class Meta:
        model = TFieldEntity
    
    # 主键本是Int，graphql会将其转化为字符串，这里强制改为int
    f_id = graphene.Int()
class PaginatedTFieldGqlObject(graphene.ObjectType):
    items = graphene.List(TFieldGqlObject)
    total = graphene.Int()
    pages = graphene.Int()
    current_page = graphene.Int()

class TFieldGqlInputObject(graphene.InputObjectType):
   
    f_id = graphene.Int(required=False)
    f_name = graphene.String(required=False)
    f_label = graphene.String(required=True)
    f_group_id = graphene.Int(required=False)
    f_icon = graphene.String(required=False)
    f_type = graphene.Int(required=False)
    f_element_id = graphene.Int(required=True)
    f_format = graphene.String(required=True)
    f_value = graphene.String(required=True)
    f_state = graphene.Int(required=True)
    f_create_time = graphene.String(required=False)
    f_modify_time = graphene.String(required=True)

