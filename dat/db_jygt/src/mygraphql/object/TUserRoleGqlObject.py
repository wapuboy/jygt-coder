#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
from graphene_sqlalchemy import SQLAlchemyObjectType
import graphene

from entity.TUserRoleEntity import TUserRoleEntity

class TUserRoleGqlObject(SQLAlchemyObjectType):
    class Meta:
        model = TUserRoleEntity
    
    # 主键本是Int，graphql会将其转化为字符串，这里强制改为int
    f_id = graphene.Int()
class PaginatedTUserRoleGqlObject(graphene.ObjectType):
    items = graphene.List(TUserRoleGqlObject)
    total = graphene.Int()
    pages = graphene.Int()
    current_page = graphene.Int()

class TUserRoleGqlInputObject(graphene.InputObjectType):
   
    f_id = graphene.Int(required=False)
    f_user_id = graphene.Int(required=False)
    f_role_id = graphene.Int(required=False)
    f_desc = graphene.String(required=True)
    f_state = graphene.Int(required=False)
    f_create_time = graphene.String(required=False)
    f_modify_time = graphene.String(required=False)

