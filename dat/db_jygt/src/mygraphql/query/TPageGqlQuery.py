#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TPageGqlObject import TPageGqlObject,PaginatedTPageGqlObject
from service.TPageService import TPageService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TPageGqlQuery(graphene.ObjectType):

    getAllTPage = graphene.Field(PaginatedTPageGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TPageGqlObject数据，支持分页")

    def resolve_getAllTPage(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTPageGqlObject: page={page}, number={number}")
        objService = TPageService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTPage=graphene.Field(
            TPageGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_page索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TPageEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTPage(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTPageGqlObject: f_id")
        objService = TPageService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxNameOfTPage=graphene.List(
            TPageGqlObject,
            para_f_name=graphene.String(),
            description="""
                根据 表t_page索引 idx_name  获取记录。

                输入参数：
                        f_name
                
                输出参数：
                        TPageEntity列表或空
                """
    )
    def resolve_getByIdxNameOfTPage(self, info, para_f_name):
        logger.info(f"{self.__class__.__name__}.getByIdxNameOfTPageGqlObject: f_name")
        objService = TPageService() 
        return objService.getByIdxName(
            para_f_name
        )
        
    getByIdxLabelOfTPage=graphene.List(
            TPageGqlObject,
            para_f_label=graphene.String(),
            description="""
                根据 表t_page索引 idx_label  获取记录。

                输入参数：
                        f_label
                
                输出参数：
                        TPageEntity列表或空
                """
    )
    def resolve_getByIdxLabelOfTPage(self, info, para_f_label):
        logger.info(f"{self.__class__.__name__}.getByIdxLabelOfTPageGqlObject: f_label")
        objService = TPageService() 
        return objService.getByIdxLabel(
            para_f_label
        )

