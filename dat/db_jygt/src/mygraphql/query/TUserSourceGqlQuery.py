#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserSourceGqlObject import TUserSourceGqlObject,PaginatedTUserSourceGqlObject
from service.TUserSourceService import TUserSourceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TUserSourceGqlQuery(graphene.ObjectType):

    getAllTUserSource = graphene.Field(PaginatedTUserSourceGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TUserSourceGqlObject数据，支持分页")

    def resolve_getAllTUserSource(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTUserSourceGqlObject: page={page}, number={number}")
        objService = TUserSourceService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTUserSource=graphene.Field(
            TUserSourceGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_user_source索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TUserSourceEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTUserSource(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTUserSourceGqlObject: f_id")
        objService = TUserSourceService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxNameOfTUserSource=graphene.List(
            TUserSourceGqlObject,
            para_f_name=graphene.String(),
            description="""
                根据 表t_user_source索引 idx_name  获取记录。

                输入参数：
                        f_name
                
                输出参数：
                        TUserSourceEntity列表或空
                """
    )
    def resolve_getByIdxNameOfTUserSource(self, info, para_f_name):
        logger.info(f"{self.__class__.__name__}.getByIdxNameOfTUserSourceGqlObject: f_name")
        objService = TUserSourceService() 
        return objService.getByIdxName(
            para_f_name
        )

