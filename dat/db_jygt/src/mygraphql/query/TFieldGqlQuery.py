#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TFieldGqlObject import TFieldGqlObject,PaginatedTFieldGqlObject
from service.TFieldService import TFieldService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TFieldGqlQuery(graphene.ObjectType):

    getAllTField = graphene.Field(PaginatedTFieldGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TFieldGqlObject数据，支持分页")

    def resolve_getAllTField(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTFieldGqlObject: page={page}, number={number}")
        objService = TFieldService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTField=graphene.Field(
            TFieldGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_field索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TFieldEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTField(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTFieldGqlObject: f_id")
        objService = TFieldService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqGroupIdAndNameOfTField=graphene.Field(
            TFieldGqlObject,
            para_f_group_id=graphene.Int(), para_f_name=graphene.String(),
            description="""
                根据 表t_field索引 unq_group_id_and_name  获取记录。

                输入参数：
                        f_group_id
                        f_name
                
                输出参数：
                        TFieldEntity对象或 None
                """
    )
    def resolve_getByUnqGroupIdAndNameOfTField(self, info, para_f_group_id, para_f_name):
        logger.info(f"{self.__class__.__name__}.getByUnqGroupIdAndNameOfTFieldGqlObject: f_group_id, f_name")
        objService = TFieldService() 
        return objService.getByUnqGroupIdAndName(
            para_f_group_id, para_f_name
        )
        
    getByIdxElementIdOfTField=graphene.List(
            TFieldGqlObject,
            para_f_element_id=graphene.Int(),
            description="""
                根据 表t_field索引 idx_element_id  获取记录。

                输入参数：
                        f_element_id
                
                输出参数：
                        TFieldEntity列表或空
                """
    )
    def resolve_getByIdxElementIdOfTField(self, info, para_f_element_id):
        logger.info(f"{self.__class__.__name__}.getByIdxElementIdOfTFieldGqlObject: f_element_id")
        objService = TFieldService() 
        return objService.getByIdxElementId(
            para_f_element_id
        )
        
    getByIdxGroupIdOfTField=graphene.List(
            TFieldGqlObject,
            para_f_group_id=graphene.Int(),
            description="""
                根据 表t_field索引 idx_group_id  获取记录。

                输入参数：
                        f_group_id
                
                输出参数：
                        TFieldEntity列表或空
                """
    )
    def resolve_getByIdxGroupIdOfTField(self, info, para_f_group_id):
        logger.info(f"{self.__class__.__name__}.getByIdxGroupIdOfTFieldGqlObject: f_group_id")
        objService = TFieldService() 
        return objService.getByIdxGroupId(
            para_f_group_id
        )
        
    getByUnqGroupIdAndLabelOfTField=graphene.List(
            TFieldGqlObject,
            para_f_group_id=graphene.Int(), para_f_label=graphene.String(),
            description="""
                根据 表t_field索引 unq_group_id_and_label  获取记录。

                输入参数：
                        f_group_id
                        f_label
                
                输出参数：
                        TFieldEntity列表或空
                """
    )
    def resolve_getByUnqGroupIdAndLabelOfTField(self, info, para_f_group_id, para_f_label):
        logger.info(f"{self.__class__.__name__}.getByUnqGroupIdAndLabelOfTFieldGqlObject: f_group_id, f_label")
        objService = TFieldService() 
        return objService.getByUnqGroupIdAndLabel(
            para_f_group_id, para_f_label
        )

