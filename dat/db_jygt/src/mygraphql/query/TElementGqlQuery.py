#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TElementGqlObject import TElementGqlObject,PaginatedTElementGqlObject
from service.TElementService import TElementService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TElementGqlQuery(graphene.ObjectType):

    getAllTElement = graphene.Field(PaginatedTElementGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TElementGqlObject数据，支持分页")

    def resolve_getAllTElement(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTElementGqlObject: page={page}, number={number}")
        objService = TElementService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTElement=graphene.Field(
            TElementGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_element索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TElementEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTElement(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTElementGqlObject: f_id")
        objService = TElementService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxGroupIdOfTElement=graphene.List(
            TElementGqlObject,
            para_f_group_id=graphene.Int(),
            description="""
                根据 表t_element索引 idx_group_id  获取记录。

                输入参数：
                        f_group_id
                
                输出参数：
                        TElementEntity列表或空
                """
    )
    def resolve_getByIdxGroupIdOfTElement(self, info, para_f_group_id):
        logger.info(f"{self.__class__.__name__}.getByIdxGroupIdOfTElementGqlObject: f_group_id")
        objService = TElementService() 
        return objService.getByIdxGroupId(
            para_f_group_id
        )

