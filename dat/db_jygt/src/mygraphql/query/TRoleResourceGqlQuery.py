#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TRoleResourceGqlObject import TRoleResourceGqlObject,PaginatedTRoleResourceGqlObject
from service.TRoleResourceService import TRoleResourceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TRoleResourceGqlQuery(graphene.ObjectType):

    getAllTRoleResource = graphene.Field(PaginatedTRoleResourceGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TRoleResourceGqlObject数据，支持分页")

    def resolve_getAllTRoleResource(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTRoleResourceGqlObject: page={page}, number={number}")
        objService = TRoleResourceService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTRoleResource=graphene.Field(
            TRoleResourceGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_role_resource索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TRoleResourceEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTRoleResource(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTRoleResourceGqlObject: f_id")
        objService = TRoleResourceService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqRoleIdAndResourceIdOfTRoleResource=graphene.Field(
            TRoleResourceGqlObject,
            para_f_role_id=graphene.Int(), para_f_resource_id=graphene.Int(),
            description="""
                根据 表t_role_resource索引 unq_role_id_and_resource_id  获取记录。

                输入参数：
                        f_role_id
                        f_resource_id
                
                输出参数：
                        TRoleResourceEntity对象或 None
                """
    )
    def resolve_getByUnqRoleIdAndResourceIdOfTRoleResource(self, info, para_f_role_id, para_f_resource_id):
        logger.info(f"{self.__class__.__name__}.getByUnqRoleIdAndResourceIdOfTRoleResourceGqlObject: f_role_id, f_resource_id")
        objService = TRoleResourceService() 
        return objService.getByUnqRoleIdAndResourceId(
            para_f_role_id, para_f_resource_id
        )
        
    getByIdxResourceIdOfTRoleResource=graphene.List(
            TRoleResourceGqlObject,
            para_f_resource_id=graphene.Int(),
            description="""
                根据 表t_role_resource索引 idx_resource_id  获取记录。

                输入参数：
                        f_resource_id
                
                输出参数：
                        TRoleResourceEntity列表或空
                """
    )
    def resolve_getByIdxResourceIdOfTRoleResource(self, info, para_f_resource_id):
        logger.info(f"{self.__class__.__name__}.getByIdxResourceIdOfTRoleResourceGqlObject: f_resource_id")
        objService = TRoleResourceService() 
        return objService.getByIdxResourceId(
            para_f_resource_id
        )
        
    getByIdxRoleIdOfTRoleResource=graphene.List(
            TRoleResourceGqlObject,
            para_f_role_id=graphene.Int(),
            description="""
                根据 表t_role_resource索引 idx_role_id  获取记录。

                输入参数：
                        f_role_id
                
                输出参数：
                        TRoleResourceEntity列表或空
                """
    )
    def resolve_getByIdxRoleIdOfTRoleResource(self, info, para_f_role_id):
        logger.info(f"{self.__class__.__name__}.getByIdxRoleIdOfTRoleResourceGqlObject: f_role_id")
        objService = TRoleResourceService() 
        return objService.getByIdxRoleId(
            para_f_role_id
        )

