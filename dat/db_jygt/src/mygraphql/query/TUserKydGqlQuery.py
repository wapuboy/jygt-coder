#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserKydGqlObject import TUserKydGqlObject,PaginatedTUserKydGqlObject
from service.TUserKydService import TUserKydService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TUserKydGqlQuery(graphene.ObjectType):

    getAllTUserKyd = graphene.Field(PaginatedTUserKydGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TUserKydGqlObject数据，支持分页")

    def resolve_getAllTUserKyd(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTUserKydGqlObject: page={page}, number={number}")
        objService = TUserKydService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTUserKyd=graphene.Field(
            TUserKydGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_user_kyd索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TUserKydEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTUserKyd(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTUserKydGqlObject: f_id")
        objService = TUserKydService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxUidOfTUserKyd=graphene.List(
            TUserKydGqlObject,
            para_f_user_id=graphene.Int(),
            description="""
                根据 表t_user_kyd索引 idx_uid  获取记录。

                输入参数：
                        f_user_id
                
                输出参数：
                        TUserKydEntity列表或空
                """
    )
    def resolve_getByIdxUidOfTUserKyd(self, info, para_f_user_id):
        logger.info(f"{self.__class__.__name__}.getByIdxUidOfTUserKydGqlObject: f_user_id")
        objService = TUserKydService() 
        return objService.getByIdxUid(
            para_f_user_id
        )
        
    getByIdxFidOfTUserKyd=graphene.List(
            TUserKydGqlObject,
            para_f_field_id=graphene.Int(),
            description="""
                根据 表t_user_kyd索引 idx_fid  获取记录。

                输入参数：
                        f_field_id
                
                输出参数：
                        TUserKydEntity列表或空
                """
    )
    def resolve_getByIdxFidOfTUserKyd(self, info, para_f_field_id):
        logger.info(f"{self.__class__.__name__}.getByIdxFidOfTUserKydGqlObject: f_field_id")
        objService = TUserKydService() 
        return objService.getByIdxFid(
            para_f_field_id
        )

