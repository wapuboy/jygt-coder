#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TGroupGqlObject import TGroupGqlObject,PaginatedTGroupGqlObject
from service.TGroupService import TGroupService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TGroupGqlQuery(graphene.ObjectType):

    getAllTGroup = graphene.Field(PaginatedTGroupGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TGroupGqlObject数据，支持分页")

    def resolve_getAllTGroup(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTGroupGqlObject: page={page}, number={number}")
        objService = TGroupService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTGroup=graphene.Field(
            TGroupGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_group索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TGroupEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTGroup(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTGroupGqlObject: f_id")
        objService = TGroupService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqTypeAndNameOfTGroup=graphene.Field(
            TGroupGqlObject,
            para_f_name=graphene.String(), para_f_type=graphene.Int(),
            description="""
                根据 表t_group索引 unq_type_and_name  获取记录。

                输入参数：
                        f_name
                        f_type
                
                输出参数：
                        TGroupEntity对象或 None
                """
    )
    def resolve_getByUnqTypeAndNameOfTGroup(self, info, para_f_name, para_f_type):
        logger.info(f"{self.__class__.__name__}.getByUnqTypeAndNameOfTGroupGqlObject: f_name, f_type")
        objService = TGroupService() 
        return objService.getByUnqTypeAndName(
            para_f_name, para_f_type
        )
        
    getByIdxNameOfTGroup=graphene.List(
            TGroupGqlObject,
            para_f_name=graphene.String(),
            description="""
                根据 表t_group索引 idx_name  获取记录。

                输入参数：
                        f_name
                
                输出参数：
                        TGroupEntity列表或空
                """
    )
    def resolve_getByIdxNameOfTGroup(self, info, para_f_name):
        logger.info(f"{self.__class__.__name__}.getByIdxNameOfTGroupGqlObject: f_name")
        objService = TGroupService() 
        return objService.getByIdxName(
            para_f_name
        )
        
    getByUnqTypeAndLabelOfTGroup=graphene.List(
            TGroupGqlObject,
            para_f_label=graphene.String(), para_f_type=graphene.Int(),
            description="""
                根据 表t_group索引 unq_type_and_label  获取记录。

                输入参数：
                        f_label
                        f_type
                
                输出参数：
                        TGroupEntity列表或空
                """
    )
    def resolve_getByUnqTypeAndLabelOfTGroup(self, info, para_f_label, para_f_type):
        logger.info(f"{self.__class__.__name__}.getByUnqTypeAndLabelOfTGroupGqlObject: f_label, f_type")
        objService = TGroupService() 
        return objService.getByUnqTypeAndLabel(
            para_f_label, para_f_type
        )

