#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TMenuGqlObject import TMenuGqlObject,PaginatedTMenuGqlObject
from service.TMenuService import TMenuService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TMenuGqlQuery(graphene.ObjectType):

    getAllTMenu = graphene.Field(PaginatedTMenuGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TMenuGqlObject数据，支持分页")

    def resolve_getAllTMenu(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTMenuGqlObject: page={page}, number={number}")
        objService = TMenuService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTMenu=graphene.Field(
            TMenuGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_menu索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TMenuEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTMenu(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTMenuGqlObject: f_id")
        objService = TMenuService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxNameOfTMenu=graphene.List(
            TMenuGqlObject,
            para_f_name=graphene.String(),
            description="""
                根据 表t_menu索引 idx_name  获取记录。

                输入参数：
                        f_name
                
                输出参数：
                        TMenuEntity列表或空
                """
    )
    def resolve_getByIdxNameOfTMenu(self, info, para_f_name):
        logger.info(f"{self.__class__.__name__}.getByIdxNameOfTMenuGqlObject: f_name")
        objService = TMenuService() 
        return objService.getByIdxName(
            para_f_name
        )
        
    getByIdxParentIdOfTMenu=graphene.List(
            TMenuGqlObject,
            para_f_parent_id=graphene.Int(),
            description="""
                根据 表t_menu索引 idx_parent_id  获取记录。

                输入参数：
                        f_parent_id
                
                输出参数：
                        TMenuEntity列表或空
                """
    )
    def resolve_getByIdxParentIdOfTMenu(self, info, para_f_parent_id):
        logger.info(f"{self.__class__.__name__}.getByIdxParentIdOfTMenuGqlObject: f_parent_id")
        objService = TMenuService() 
        return objService.getByIdxParentId(
            para_f_parent_id
        )

