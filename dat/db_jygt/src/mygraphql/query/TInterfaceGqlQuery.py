#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TInterfaceGqlObject import TInterfaceGqlObject,PaginatedTInterfaceGqlObject
from service.TInterfaceService import TInterfaceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TInterfaceGqlQuery(graphene.ObjectType):

    getAllTInterface = graphene.Field(PaginatedTInterfaceGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TInterfaceGqlObject数据，支持分页")

    def resolve_getAllTInterface(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTInterfaceGqlObject: page={page}, number={number}")
        objService = TInterfaceService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTInterface=graphene.Field(
            TInterfaceGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_interface索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TInterfaceEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTInterface(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTInterfaceGqlObject: f_id")
        objService = TInterfaceService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxNameOfTInterface=graphene.List(
            TInterfaceGqlObject,
            para_f_name=graphene.String(),
            description="""
                根据 表t_interface索引 idx_name  获取记录。

                输入参数：
                        f_name
                
                输出参数：
                        TInterfaceEntity列表或空
                """
    )
    def resolve_getByIdxNameOfTInterface(self, info, para_f_name):
        logger.info(f"{self.__class__.__name__}.getByIdxNameOfTInterfaceGqlObject: f_name")
        objService = TInterfaceService() 
        return objService.getByIdxName(
            para_f_name
        )
        
    getByIdxLabelOfTInterface=graphene.List(
            TInterfaceGqlObject,
            para_f_label=graphene.String(),
            description="""
                根据 表t_interface索引 idx_label  获取记录。

                输入参数：
                        f_label
                
                输出参数：
                        TInterfaceEntity列表或空
                """
    )
    def resolve_getByIdxLabelOfTInterface(self, info, para_f_label):
        logger.info(f"{self.__class__.__name__}.getByIdxLabelOfTInterfaceGqlObject: f_label")
        objService = TInterfaceService() 
        return objService.getByIdxLabel(
            para_f_label
        )

