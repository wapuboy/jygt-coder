#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TResourceGqlObject import TResourceGqlObject,PaginatedTResourceGqlObject
from service.TResourceService import TResourceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TResourceGqlQuery(graphene.ObjectType):

    getAllTResource = graphene.Field(PaginatedTResourceGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TResourceGqlObject数据，支持分页")

    def resolve_getAllTResource(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTResourceGqlObject: page={page}, number={number}")
        objService = TResourceService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTResource=graphene.Field(
            TResourceGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_resource索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TResourceEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTResource(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTResourceGqlObject: f_id")
        objService = TResourceService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqTypeAndResourceIdOfTResource=graphene.Field(
            TResourceGqlObject,
            para_f_resource_type=graphene.Int(), para_f_resource_id=graphene.Int(),
            description="""
                根据 表t_resource索引 unq_type_and_resource_id  获取记录。

                输入参数：
                        f_resource_type
                        f_resource_id
                
                输出参数：
                        TResourceEntity对象或 None
                """
    )
    def resolve_getByUnqTypeAndResourceIdOfTResource(self, info, para_f_resource_type, para_f_resource_id):
        logger.info(f"{self.__class__.__name__}.getByUnqTypeAndResourceIdOfTResourceGqlObject: f_resource_type, f_resource_id")
        objService = TResourceService() 
        return objService.getByUnqTypeAndResourceId(
            para_f_resource_type, para_f_resource_id
        )
        
    getByIdxResourceIdOfTResource=graphene.List(
            TResourceGqlObject,
            para_f_resource_id=graphene.Int(),
            description="""
                根据 表t_resource索引 idx_resource_id  获取记录。

                输入参数：
                        f_resource_id
                
                输出参数：
                        TResourceEntity列表或空
                """
    )
    def resolve_getByIdxResourceIdOfTResource(self, info, para_f_resource_id):
        logger.info(f"{self.__class__.__name__}.getByIdxResourceIdOfTResourceGqlObject: f_resource_id")
        objService = TResourceService() 
        return objService.getByIdxResourceId(
            para_f_resource_id
        )
        
    getByIdxTypeOfTResource=graphene.List(
            TResourceGqlObject,
            para_f_resource_type=graphene.Int(),
            description="""
                根据 表t_resource索引 idx_type  获取记录。

                输入参数：
                        f_resource_type
                
                输出参数：
                        TResourceEntity列表或空
                """
    )
    def resolve_getByIdxTypeOfTResource(self, info, para_f_resource_type):
        logger.info(f"{self.__class__.__name__}.getByIdxTypeOfTResourceGqlObject: f_resource_type")
        objService = TResourceService() 
        return objService.getByIdxType(
            para_f_resource_type
        )

