#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TKyxTemplateGqlObject import TKyxTemplateGqlObject,PaginatedTKyxTemplateGqlObject
from service.TKyxTemplateService import TKyxTemplateService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TKyxTemplateGqlQuery(graphene.ObjectType):

    getAllTKyxTemplate = graphene.Field(PaginatedTKyxTemplateGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TKyxTemplateGqlObject数据，支持分页")

    def resolve_getAllTKyxTemplate(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTKyxTemplateGqlObject: page={page}, number={number}")
        objService = TKyxTemplateService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTKyxTemplate=graphene.Field(
            TKyxTemplateGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_kyx_template索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TKyxTemplateEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTKyxTemplate(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTKyxTemplateGqlObject: f_id")
        objService = TKyxTemplateService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqTypeAndEntityIdAndFieldIdOfTKyxTemplate=graphene.Field(
            TKyxTemplateGqlObject,
            para_f_entity_type=graphene.Int(), para_f_entity_id=graphene.Int(), para_f_field_id=graphene.Int(),
            description="""
                根据 表t_kyx_template索引 unq_type_and_entity_id_and_field_id  获取记录。

                输入参数：
                        f_entity_type
                        f_entity_id
                        f_field_id
                
                输出参数：
                        TKyxTemplateEntity对象或 None
                """
    )
    def resolve_getByUnqTypeAndEntityIdAndFieldIdOfTKyxTemplate(self, info, para_f_entity_type, para_f_entity_id, para_f_field_id):
        logger.info(f"{self.__class__.__name__}.getByUnqTypeAndEntityIdAndFieldIdOfTKyxTemplateGqlObject: f_entity_type, f_entity_id, f_field_id")
        objService = TKyxTemplateService() 
        return objService.getByUnqTypeAndEntityIdAndFieldId(
            para_f_entity_type, para_f_entity_id, para_f_field_id
        )
        
    getByIdxTypeAndEntityIdOfTKyxTemplate=graphene.List(
            TKyxTemplateGqlObject,
            para_f_entity_id=graphene.Int(), para_f_entity_type=graphene.Int(),
            description="""
                根据 表t_kyx_template索引 idx_type_and_entity_id  获取记录。

                输入参数：
                        f_entity_id
                        f_entity_type
                
                输出参数：
                        TKyxTemplateEntity列表或空
                """
    )
    def resolve_getByIdxTypeAndEntityIdOfTKyxTemplate(self, info, para_f_entity_id, para_f_entity_type):
        logger.info(f"{self.__class__.__name__}.getByIdxTypeAndEntityIdOfTKyxTemplateGqlObject: f_entity_id, f_entity_type")
        objService = TKyxTemplateService() 
        return objService.getByIdxTypeAndEntityId(
            para_f_entity_id, para_f_entity_type
        )

