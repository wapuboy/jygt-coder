#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TRelationGqlObject import TRelationGqlObject,PaginatedTRelationGqlObject
from service.TRelationService import TRelationService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TRelationGqlQuery(graphene.ObjectType):

    getAllTRelation = graphene.Field(PaginatedTRelationGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TRelationGqlObject数据，支持分页")

    def resolve_getAllTRelation(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTRelationGqlObject: page={page}, number={number}")
        objService = TRelationService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTRelation=graphene.Field(
            TRelationGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_relation索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TRelationEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTRelation(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTRelationGqlObject: f_id")
        objService = TRelationService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqNameOfTRelation=graphene.List(
            TRelationGqlObject,
            para_f_name=graphene.String(),
            description="""
                根据 表t_relation索引 unq_name  获取记录。

                输入参数：
                        f_name
                
                输出参数：
                        TRelationEntity列表或空
                """
    )
    def resolve_getByUnqNameOfTRelation(self, info, para_f_name):
        logger.info(f"{self.__class__.__name__}.getByUnqNameOfTRelationGqlObject: f_name")
        objService = TRelationService() 
        return objService.getByUnqName(
            para_f_name
        )
        
    getByIdxTypeOfTRelation=graphene.List(
            TRelationGqlObject,
            para_f_type=graphene.Int(),
            description="""
                根据 表t_relation索引 idx_type  获取记录。

                输入参数：
                        f_type
                
                输出参数：
                        TRelationEntity列表或空
                """
    )
    def resolve_getByIdxTypeOfTRelation(self, info, para_f_type):
        logger.info(f"{self.__class__.__name__}.getByIdxTypeOfTRelationGqlObject: f_type")
        objService = TRelationService() 
        return objService.getByIdxType(
            para_f_type
        )

