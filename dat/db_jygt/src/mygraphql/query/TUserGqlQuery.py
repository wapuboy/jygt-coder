#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserGqlObject import TUserGqlObject,PaginatedTUserGqlObject
from service.TUserService import TUserService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TUserGqlQuery(graphene.ObjectType):

    getAllTUser = graphene.Field(PaginatedTUserGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TUserGqlObject数据，支持分页")

    def resolve_getAllTUser(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTUserGqlObject: page={page}, number={number}")
        objService = TUserService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTUser=graphene.Field(
            TUserGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_user索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TUserEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTUser(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTUserGqlObject: f_id")
        objService = TUserService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqSourceAndUidOfTUser=graphene.Field(
            TUserGqlObject,
            para_f_uid=graphene.String(), para_f_user_source_id=graphene.Int(),
            description="""
                根据 表t_user索引 unq_source_and_uid  获取记录。

                输入参数：
                        f_uid
                        f_user_source_id
                
                输出参数：
                        TUserEntity对象或 None
                """
    )
    def resolve_getByUnqSourceAndUidOfTUser(self, info, para_f_uid, para_f_user_source_id):
        logger.info(f"{self.__class__.__name__}.getByUnqSourceAndUidOfTUserGqlObject: f_uid, f_user_source_id")
        objService = TUserService() 
        return objService.getByUnqSourceAndUid(
            para_f_uid, para_f_user_source_id
        )
        
    getByIdxSourceOfTUser=graphene.List(
            TUserGqlObject,
            para_f_user_source_id=graphene.Int(),
            description="""
                根据 表t_user索引 idx_source  获取记录。

                输入参数：
                        f_user_source_id
                
                输出参数：
                        TUserEntity列表或空
                """
    )
    def resolve_getByIdxSourceOfTUser(self, info, para_f_user_source_id):
        logger.info(f"{self.__class__.__name__}.getByIdxSourceOfTUserGqlObject: f_user_source_id")
        objService = TUserService() 
        return objService.getByIdxSource(
            para_f_user_source_id
        )

