#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserRelationGqlObject import TUserRelationGqlObject,PaginatedTUserRelationGqlObject
from service.TUserRelationService import TUserRelationService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TUserRelationGqlQuery(graphene.ObjectType):

    getAllTUserRelation = graphene.Field(PaginatedTUserRelationGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TUserRelationGqlObject数据，支持分页")

    def resolve_getAllTUserRelation(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTUserRelationGqlObject: page={page}, number={number}")
        objService = TUserRelationService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTUserRelation=graphene.Field(
            TUserRelationGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_user_relation索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TUserRelationEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTUserRelation(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTUserRelationGqlObject: f_id")
        objService = TUserRelationService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqUid1AndRidAndUid2OfTUserRelation=graphene.Field(
            TUserRelationGqlObject,
            para_f_user_id_1=graphene.Int(), para_f_relation_id=graphene.Int(), para_f_user_id_2=graphene.Int(),
            description="""
                根据 表t_user_relation索引 unq_uid1_and_rid_and_uid2  获取记录。

                输入参数：
                        f_user_id_1
                        f_relation_id
                        f_user_id_2
                
                输出参数：
                        TUserRelationEntity对象或 None
                """
    )
    def resolve_getByUnqUid1AndRidAndUid2OfTUserRelation(self, info, para_f_user_id_1, para_f_relation_id, para_f_user_id_2):
        logger.info(f"{self.__class__.__name__}.getByUnqUid1AndRidAndUid2OfTUserRelationGqlObject: f_user_id_1, f_relation_id, f_user_id_2")
        objService = TUserRelationService() 
        return objService.getByUnqUid1AndRidAndUid2(
            para_f_user_id_1, para_f_relation_id, para_f_user_id_2
        )
        
    getByIdxUid1OfTUserRelation=graphene.List(
            TUserRelationGqlObject,
            para_f_user_id_1=graphene.Int(),
            description="""
                根据 表t_user_relation索引 idx_uid1  获取记录。

                输入参数：
                        f_user_id_1
                
                输出参数：
                        TUserRelationEntity列表或空
                """
    )
    def resolve_getByIdxUid1OfTUserRelation(self, info, para_f_user_id_1):
        logger.info(f"{self.__class__.__name__}.getByIdxUid1OfTUserRelationGqlObject: f_user_id_1")
        objService = TUserRelationService() 
        return objService.getByIdxUid1(
            para_f_user_id_1
        )
        
    getByIdxUid1AndRidOfTUserRelation=graphene.List(
            TUserRelationGqlObject,
            para_f_user_id_1=graphene.Int(), para_f_relation_id=graphene.Int(),
            description="""
                根据 表t_user_relation索引 idx_uid1_and_rid  获取记录。

                输入参数：
                        f_user_id_1
                        f_relation_id
                
                输出参数：
                        TUserRelationEntity列表或空
                """
    )
    def resolve_getByIdxUid1AndRidOfTUserRelation(self, info, para_f_user_id_1, para_f_relation_id):
        logger.info(f"{self.__class__.__name__}.getByIdxUid1AndRidOfTUserRelationGqlObject: f_user_id_1, f_relation_id")
        objService = TUserRelationService() 
        return objService.getByIdxUid1AndRid(
            para_f_user_id_1, para_f_relation_id
        )
        
    getByIdxUid2OfTUserRelation=graphene.List(
            TUserRelationGqlObject,
            para_f_user_id_2=graphene.Int(),
            description="""
                根据 表t_user_relation索引 idx_uid2  获取记录。

                输入参数：
                        f_user_id_2
                
                输出参数：
                        TUserRelationEntity列表或空
                """
    )
    def resolve_getByIdxUid2OfTUserRelation(self, info, para_f_user_id_2):
        logger.info(f"{self.__class__.__name__}.getByIdxUid2OfTUserRelationGqlObject: f_user_id_2")
        objService = TUserRelationService() 
        return objService.getByIdxUid2(
            para_f_user_id_2
        )
        
    getByIdxUid2AndRidOfTUserRelation=graphene.List(
            TUserRelationGqlObject,
            para_f_relation_id=graphene.Int(), para_f_user_id_2=graphene.Int(),
            description="""
                根据 表t_user_relation索引 idx_uid2_and_rid  获取记录。

                输入参数：
                        f_relation_id
                        f_user_id_2
                
                输出参数：
                        TUserRelationEntity列表或空
                """
    )
    def resolve_getByIdxUid2AndRidOfTUserRelation(self, info, para_f_relation_id, para_f_user_id_2):
        logger.info(f"{self.__class__.__name__}.getByIdxUid2AndRidOfTUserRelationGqlObject: f_relation_id, f_user_id_2")
        objService = TUserRelationService() 
        return objService.getByIdxUid2AndRid(
            para_f_relation_id, para_f_user_id_2
        )

