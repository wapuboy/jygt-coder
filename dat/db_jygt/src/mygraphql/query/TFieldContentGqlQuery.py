#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TFieldContentGqlObject import TFieldContentGqlObject,PaginatedTFieldContentGqlObject
from service.TFieldContentService import TFieldContentService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TFieldContentGqlQuery(graphene.ObjectType):

    getAllTFieldContent = graphene.Field(PaginatedTFieldContentGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TFieldContentGqlObject数据，支持分页")

    def resolve_getAllTFieldContent(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTFieldContentGqlObject: page={page}, number={number}")
        objService = TFieldContentService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTFieldContent=graphene.Field(
            TFieldContentGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_field_content索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TFieldContentEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTFieldContent(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTFieldContentGqlObject: f_id")
        objService = TFieldContentService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqFieldIdAndContentIdOfTFieldContent=graphene.Field(
            TFieldContentGqlObject,
            para_f_content_id=graphene.Int(), para_f_field_id=graphene.Int(),
            description="""
                根据 表t_field_content索引 unq_field_id_and_content_id  获取记录。

                输入参数：
                        f_content_id
                        f_field_id
                
                输出参数：
                        TFieldContentEntity对象或 None
                """
    )
    def resolve_getByUnqFieldIdAndContentIdOfTFieldContent(self, info, para_f_content_id, para_f_field_id):
        logger.info(f"{self.__class__.__name__}.getByUnqFieldIdAndContentIdOfTFieldContentGqlObject: f_content_id, f_field_id")
        objService = TFieldContentService() 
        return objService.getByUnqFieldIdAndContentId(
            para_f_content_id, para_f_field_id
        )
        
    getByIdxContentIdOfTFieldContent=graphene.List(
            TFieldContentGqlObject,
            para_f_content_id=graphene.Int(),
            description="""
                根据 表t_field_content索引 idx_content_id  获取记录。

                输入参数：
                        f_content_id
                
                输出参数：
                        TFieldContentEntity列表或空
                """
    )
    def resolve_getByIdxContentIdOfTFieldContent(self, info, para_f_content_id):
        logger.info(f"{self.__class__.__name__}.getByIdxContentIdOfTFieldContentGqlObject: f_content_id")
        objService = TFieldContentService() 
        return objService.getByIdxContentId(
            para_f_content_id
        )
        
    getByIdxFieldIdOfTFieldContent=graphene.List(
            TFieldContentGqlObject,
            para_f_field_id=graphene.Int(),
            description="""
                根据 表t_field_content索引 idx_field_id  获取记录。

                输入参数：
                        f_field_id
                
                输出参数：
                        TFieldContentEntity列表或空
                """
    )
    def resolve_getByIdxFieldIdOfTFieldContent(self, info, para_f_field_id):
        logger.info(f"{self.__class__.__name__}.getByIdxFieldIdOfTFieldContentGqlObject: f_field_id")
        objService = TFieldContentService() 
        return objService.getByIdxFieldId(
            para_f_field_id
        )

