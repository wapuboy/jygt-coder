#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserLogGqlObject import TUserLogGqlObject,PaginatedTUserLogGqlObject
from service.TUserLogService import TUserLogService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TUserLogGqlQuery(graphene.ObjectType):

    getAllTUserLog = graphene.Field(PaginatedTUserLogGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TUserLogGqlObject数据，支持分页")

    def resolve_getAllTUserLog(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTUserLogGqlObject: page={page}, number={number}")
        objService = TUserLogService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTUserLog=graphene.Field(
            TUserLogGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_user_log索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TUserLogEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTUserLog(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTUserLogGqlObject: f_id")
        objService = TUserLogService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxUidOfTUserLog=graphene.List(
            TUserLogGqlObject,
            para_f_user_id=graphene.Int(),
            description="""
                根据 表t_user_log索引 idx_uid  获取记录。

                输入参数：
                        f_user_id
                
                输出参数：
                        TUserLogEntity列表或空
                """
    )
    def resolve_getByIdxUidOfTUserLog(self, info, para_f_user_id):
        logger.info(f"{self.__class__.__name__}.getByIdxUidOfTUserLogGqlObject: f_user_id")
        objService = TUserLogService() 
        return objService.getByIdxUid(
            para_f_user_id
        )

