#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserRoleGqlObject import TUserRoleGqlObject,PaginatedTUserRoleGqlObject
from service.TUserRoleService import TUserRoleService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TUserRoleGqlQuery(graphene.ObjectType):

    getAllTUserRole = graphene.Field(PaginatedTUserRoleGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TUserRoleGqlObject数据，支持分页")

    def resolve_getAllTUserRole(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTUserRoleGqlObject: page={page}, number={number}")
        objService = TUserRoleService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTUserRole=graphene.Field(
            TUserRoleGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_user_role索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TUserRoleEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTUserRole(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTUserRoleGqlObject: f_id")
        objService = TUserRoleService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqUidAndRoleIdOfTUserRole=graphene.Field(
            TUserRoleGqlObject,
            para_f_user_id=graphene.Int(), para_f_role_id=graphene.Int(),
            description="""
                根据 表t_user_role索引 unq_uid_and_role_id  获取记录。

                输入参数：
                        f_user_id
                        f_role_id
                
                输出参数：
                        TUserRoleEntity对象或 None
                """
    )
    def resolve_getByUnqUidAndRoleIdOfTUserRole(self, info, para_f_user_id, para_f_role_id):
        logger.info(f"{self.__class__.__name__}.getByUnqUidAndRoleIdOfTUserRoleGqlObject: f_user_id, f_role_id")
        objService = TUserRoleService() 
        return objService.getByUnqUidAndRoleId(
            para_f_user_id, para_f_role_id
        )
        
    getByIdxUidOfTUserRole=graphene.List(
            TUserRoleGqlObject,
            para_f_user_id=graphene.Int(),
            description="""
                根据 表t_user_role索引 idx_uid  获取记录。

                输入参数：
                        f_user_id
                
                输出参数：
                        TUserRoleEntity列表或空
                """
    )
    def resolve_getByIdxUidOfTUserRole(self, info, para_f_user_id):
        logger.info(f"{self.__class__.__name__}.getByIdxUidOfTUserRoleGqlObject: f_user_id")
        objService = TUserRoleService() 
        return objService.getByIdxUid(
            para_f_user_id
        )
        
    getByIdxRoleIdOfTUserRole=graphene.List(
            TUserRoleGqlObject,
            para_f_role_id=graphene.Int(),
            description="""
                根据 表t_user_role索引 idx_role_id  获取记录。

                输入参数：
                        f_role_id
                
                输出参数：
                        TUserRoleEntity列表或空
                """
    )
    def resolve_getByIdxRoleIdOfTUserRole(self, info, para_f_role_id):
        logger.info(f"{self.__class__.__name__}.getByIdxRoleIdOfTUserRoleGqlObject: f_role_id")
        objService = TUserRoleService() 
        return objService.getByIdxRoleId(
            para_f_role_id
        )

