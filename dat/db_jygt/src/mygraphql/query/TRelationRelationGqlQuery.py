#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TRelationRelationGqlObject import TRelationRelationGqlObject,PaginatedTRelationRelationGqlObject
from service.TRelationRelationService import TRelationRelationService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TRelationRelationGqlQuery(graphene.ObjectType):

    getAllTRelationRelation = graphene.Field(PaginatedTRelationRelationGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TRelationRelationGqlObject数据，支持分页")

    def resolve_getAllTRelationRelation(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTRelationRelationGqlObject: page={page}, number={number}")
        objService = TRelationRelationService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTRelationRelation=graphene.Field(
            TRelationRelationGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_relation_relation索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TRelationRelationEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTRelationRelation(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTRelationRelationGqlObject: f_id")
        objService = TRelationRelationService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByUnqR1AndTypeAndR2OfTRelationRelation=graphene.Field(
            TRelationRelationGqlObject,
            para_f_relation_id_1=graphene.Int(), para_f_type=graphene.Int(), para_f_relation_id_2=graphene.Int(),
            description="""
                根据 表t_relation_relation索引 unq_r1_and_type_and_r2  获取记录。

                输入参数：
                        f_relation_id_1
                        f_type
                        f_relation_id_2
                
                输出参数：
                        TRelationRelationEntity对象或 None
                """
    )
    def resolve_getByUnqR1AndTypeAndR2OfTRelationRelation(self, info, para_f_relation_id_1, para_f_type, para_f_relation_id_2):
        logger.info(f"{self.__class__.__name__}.getByUnqR1AndTypeAndR2OfTRelationRelationGqlObject: f_relation_id_1, f_type, f_relation_id_2")
        objService = TRelationRelationService() 
        return objService.getByUnqR1AndTypeAndR2(
            para_f_relation_id_1, para_f_type, para_f_relation_id_2
        )
        
    getByIdxR2OfTRelationRelation=graphene.List(
            TRelationRelationGqlObject,
            para_f_relation_id_2=graphene.Int(),
            description="""
                根据 表t_relation_relation索引 idx_r2  获取记录。

                输入参数：
                        f_relation_id_2
                
                输出参数：
                        TRelationRelationEntity列表或空
                """
    )
    def resolve_getByIdxR2OfTRelationRelation(self, info, para_f_relation_id_2):
        logger.info(f"{self.__class__.__name__}.getByIdxR2OfTRelationRelationGqlObject: f_relation_id_2")
        objService = TRelationRelationService() 
        return objService.getByIdxR2(
            para_f_relation_id_2
        )
        
    getByIdxR1OfTRelationRelation=graphene.List(
            TRelationRelationGqlObject,
            para_f_relation_id_1=graphene.Int(),
            description="""
                根据 表t_relation_relation索引 idx_r1  获取记录。

                输入参数：
                        f_relation_id_1
                
                输出参数：
                        TRelationRelationEntity列表或空
                """
    )
    def resolve_getByIdxR1OfTRelationRelation(self, info, para_f_relation_id_1):
        logger.info(f"{self.__class__.__name__}.getByIdxR1OfTRelationRelationGqlObject: f_relation_id_1")
        objService = TRelationRelationService() 
        return objService.getByIdxR1(
            para_f_relation_id_1
        )
        
    getByIdxR1AndTypeOfTRelationRelation=graphene.List(
            TRelationRelationGqlObject,
            para_f_relation_id_1=graphene.Int(), para_f_type=graphene.Int(),
            description="""
                根据 表t_relation_relation索引 idx_r1_and_type  获取记录。

                输入参数：
                        f_relation_id_1
                        f_type
                
                输出参数：
                        TRelationRelationEntity列表或空
                """
    )
    def resolve_getByIdxR1AndTypeOfTRelationRelation(self, info, para_f_relation_id_1, para_f_type):
        logger.info(f"{self.__class__.__name__}.getByIdxR1AndTypeOfTRelationRelationGqlObject: f_relation_id_1, f_type")
        objService = TRelationRelationService() 
        return objService.getByIdxR1AndType(
            para_f_relation_id_1, para_f_type
        )
        
    getByIdxTypeAndR2OfTRelationRelation=graphene.List(
            TRelationRelationGqlObject,
            para_f_type=graphene.Int(), para_f_relation_id_2=graphene.Int(),
            description="""
                根据 表t_relation_relation索引 idx_type_and_r2  获取记录。

                输入参数：
                        f_type
                        f_relation_id_2
                
                输出参数：
                        TRelationRelationEntity列表或空
                """
    )
    def resolve_getByIdxTypeAndR2OfTRelationRelation(self, info, para_f_type, para_f_relation_id_2):
        logger.info(f"{self.__class__.__name__}.getByIdxTypeAndR2OfTRelationRelationGqlObject: f_type, f_relation_id_2")
        objService = TRelationRelationService() 
        return objService.getByIdxTypeAndR2(
            para_f_type, para_f_relation_id_2
        )

