#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TButtonGqlObject import TButtonGqlObject,PaginatedTButtonGqlObject
from service.TButtonService import TButtonService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TButtonGqlQuery(graphene.ObjectType):

    getAllTButton = graphene.Field(PaginatedTButtonGqlObject,page=graphene.Int(),number=graphene.Int(),description = "获取所有的TButtonGqlObject数据，支持分页")

    def resolve_getAllTButton(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAllTButtonGqlObject: page={page}, number={number}")
        objService = TButtonService()
        return objService.getAll(page, number)

        
    getByPrimaryOfTButton=graphene.Field(
            TButtonGqlObject,
            para_f_id=graphene.Int(),
            description="""
                根据 表t_button索引 PRIMARY  获取记录。

                输入参数：
                        f_id
                
                输出参数：
                        TButtonEntity对象或 None
                """
    )
    def resolve_getByPrimaryOfTButton(self, info, para_f_id):
        logger.info(f"{self.__class__.__name__}.getByPrimaryOfTButtonGqlObject: f_id")
        objService = TButtonService() 
        return objService.getByPrimary(
            para_f_id
        )
        
    getByIdxGroupIdOfTButton=graphene.List(
            TButtonGqlObject,
            para_f_group_id=graphene.Int(),
            description="""
                根据 表t_button索引 idx_group_id  获取记录。

                输入参数：
                        f_group_id
                
                输出参数：
                        TButtonEntity列表或空
                """
    )
    def resolve_getByIdxGroupIdOfTButton(self, info, para_f_group_id):
        logger.info(f"{self.__class__.__name__}.getByIdxGroupIdOfTButtonGqlObject: f_group_id")
        objService = TButtonService() 
        return objService.getByIdxGroupId(
            para_f_group_id
        )

