#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TInterfaceGqlObject import TInterfaceGqlObject,PaginatedTInterfaceGqlObject,TInterfaceGqlInputObject
from service.TInterfaceService import TInterfaceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTInterfaceGqlObject(graphene.Mutation):
    class Arguments:
        inTInterfaceGqlObjects = graphene.List(TInterfaceGqlInputObject,required=True)
        
    res = graphene.List(TInterfaceGqlObject)

    def mutate(self, info, inTInterfaceGqlObjects):
        
        objService = TInterfaceService()
        
        return ReplaceTInterfaceGqlObject(
            res = objService.replace(inTInterfaceGqlObjects)
        )
class AddTInterfaceGqlObject(graphene.Mutation):
    class Arguments:
        inTInterfaceGqlObjects = graphene.List(TInterfaceGqlInputObject,required=True)
        
    res = graphene.List(TInterfaceGqlObject)

    def mutate(self, info, inTInterfaceGqlObjects):
        
        objService = TInterfaceService()
        
        return AddTInterfaceGqlObject(
            res = objService.add(inTInterfaceGqlObjects)
        )
class UpdateTInterfaceGqlObject(graphene.Mutation):
    class Arguments:
        inTInterfaceGqlObjects = graphene.List(TInterfaceGqlInputObject,required=True)
        
    res = graphene.List(TInterfaceGqlObject)

    def mutate(self, info, inTInterfaceGqlObjects):
        
        objService = TInterfaceService()
        
        return UpdateTInterfaceGqlObject(
            res = objService.update(inTInterfaceGqlObjects)
        )
class DeleteTInterfaceGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TInterfaceGqlObject)

    def mutate(self, info, f_ids):
        objService = TInterfaceService()
        return DeleteTInterfaceGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTInterface = DeleteTInterfaceGqlObject.Field()

    addTInterface = AddTInterfaceGqlObject.Field()

    updateTInterface = UpdateTInterfaceGqlObject.Field()

    replaceTInterface = ReplaceTInterfaceGqlObject.Field()

