#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserRelationGqlObject import TUserRelationGqlObject,PaginatedTUserRelationGqlObject,TUserRelationGqlInputObject
from service.TUserRelationService import TUserRelationService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTUserRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTUserRelationGqlObjects = graphene.List(TUserRelationGqlInputObject,required=True)
        
    res = graphene.List(TUserRelationGqlObject)

    def mutate(self, info, inTUserRelationGqlObjects):
        
        objService = TUserRelationService()
        
        return ReplaceTUserRelationGqlObject(
            res = objService.replace(inTUserRelationGqlObjects)
        )
class AddTUserRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTUserRelationGqlObjects = graphene.List(TUserRelationGqlInputObject,required=True)
        
    res = graphene.List(TUserRelationGqlObject)

    def mutate(self, info, inTUserRelationGqlObjects):
        
        objService = TUserRelationService()
        
        return AddTUserRelationGqlObject(
            res = objService.add(inTUserRelationGqlObjects)
        )
class UpdateTUserRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTUserRelationGqlObjects = graphene.List(TUserRelationGqlInputObject,required=True)
        
    res = graphene.List(TUserRelationGqlObject)

    def mutate(self, info, inTUserRelationGqlObjects):
        
        objService = TUserRelationService()
        
        return UpdateTUserRelationGqlObject(
            res = objService.update(inTUserRelationGqlObjects)
        )
class DeleteTUserRelationGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TUserRelationGqlObject)

    def mutate(self, info, f_ids):
        objService = TUserRelationService()
        return DeleteTUserRelationGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTUserRelation = DeleteTUserRelationGqlObject.Field()

    addTUserRelation = AddTUserRelationGqlObject.Field()

    updateTUserRelation = UpdateTUserRelationGqlObject.Field()

    replaceTUserRelation = ReplaceTUserRelationGqlObject.Field()

