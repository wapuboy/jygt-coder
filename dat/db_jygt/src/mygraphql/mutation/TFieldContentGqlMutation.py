#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TFieldContentGqlObject import TFieldContentGqlObject,PaginatedTFieldContentGqlObject,TFieldContentGqlInputObject
from service.TFieldContentService import TFieldContentService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTFieldContentGqlObject(graphene.Mutation):
    class Arguments:
        inTFieldContentGqlObjects = graphene.List(TFieldContentGqlInputObject,required=True)
        
    res = graphene.List(TFieldContentGqlObject)

    def mutate(self, info, inTFieldContentGqlObjects):
        
        objService = TFieldContentService()
        
        return ReplaceTFieldContentGqlObject(
            res = objService.replace(inTFieldContentGqlObjects)
        )
class AddTFieldContentGqlObject(graphene.Mutation):
    class Arguments:
        inTFieldContentGqlObjects = graphene.List(TFieldContentGqlInputObject,required=True)
        
    res = graphene.List(TFieldContentGqlObject)

    def mutate(self, info, inTFieldContentGqlObjects):
        
        objService = TFieldContentService()
        
        return AddTFieldContentGqlObject(
            res = objService.add(inTFieldContentGqlObjects)
        )
class UpdateTFieldContentGqlObject(graphene.Mutation):
    class Arguments:
        inTFieldContentGqlObjects = graphene.List(TFieldContentGqlInputObject,required=True)
        
    res = graphene.List(TFieldContentGqlObject)

    def mutate(self, info, inTFieldContentGqlObjects):
        
        objService = TFieldContentService()
        
        return UpdateTFieldContentGqlObject(
            res = objService.update(inTFieldContentGqlObjects)
        )
class DeleteTFieldContentGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TFieldContentGqlObject)

    def mutate(self, info, f_ids):
        objService = TFieldContentService()
        return DeleteTFieldContentGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTFieldContent = DeleteTFieldContentGqlObject.Field()

    addTFieldContent = AddTFieldContentGqlObject.Field()

    updateTFieldContent = UpdateTFieldContentGqlObject.Field()

    replaceTFieldContent = ReplaceTFieldContentGqlObject.Field()

