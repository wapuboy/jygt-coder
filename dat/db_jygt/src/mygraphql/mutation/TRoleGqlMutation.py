#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TRoleGqlObject import TRoleGqlObject,PaginatedTRoleGqlObject,TRoleGqlInputObject
from service.TRoleService import TRoleService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTRoleGqlObject(graphene.Mutation):
    class Arguments:
        inTRoleGqlObjects = graphene.List(TRoleGqlInputObject,required=True)
        
    res = graphene.List(TRoleGqlObject)

    def mutate(self, info, inTRoleGqlObjects):
        
        objService = TRoleService()
        
        return ReplaceTRoleGqlObject(
            res = objService.replace(inTRoleGqlObjects)
        )
class AddTRoleGqlObject(graphene.Mutation):
    class Arguments:
        inTRoleGqlObjects = graphene.List(TRoleGqlInputObject,required=True)
        
    res = graphene.List(TRoleGqlObject)

    def mutate(self, info, inTRoleGqlObjects):
        
        objService = TRoleService()
        
        return AddTRoleGqlObject(
            res = objService.add(inTRoleGqlObjects)
        )
class UpdateTRoleGqlObject(graphene.Mutation):
    class Arguments:
        inTRoleGqlObjects = graphene.List(TRoleGqlInputObject,required=True)
        
    res = graphene.List(TRoleGqlObject)

    def mutate(self, info, inTRoleGqlObjects):
        
        objService = TRoleService()
        
        return UpdateTRoleGqlObject(
            res = objService.update(inTRoleGqlObjects)
        )
class DeleteTRoleGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TRoleGqlObject)

    def mutate(self, info, f_ids):
        objService = TRoleService()
        return DeleteTRoleGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTRole = DeleteTRoleGqlObject.Field()

    addTRole = AddTRoleGqlObject.Field()

    updateTRole = UpdateTRoleGqlObject.Field()

    replaceTRole = ReplaceTRoleGqlObject.Field()

