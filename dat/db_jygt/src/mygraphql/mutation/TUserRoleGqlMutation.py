#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserRoleGqlObject import TUserRoleGqlObject,PaginatedTUserRoleGqlObject,TUserRoleGqlInputObject
from service.TUserRoleService import TUserRoleService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTUserRoleGqlObject(graphene.Mutation):
    class Arguments:
        inTUserRoleGqlObjects = graphene.List(TUserRoleGqlInputObject,required=True)
        
    res = graphene.List(TUserRoleGqlObject)

    def mutate(self, info, inTUserRoleGqlObjects):
        
        objService = TUserRoleService()
        
        return ReplaceTUserRoleGqlObject(
            res = objService.replace(inTUserRoleGqlObjects)
        )
class AddTUserRoleGqlObject(graphene.Mutation):
    class Arguments:
        inTUserRoleGqlObjects = graphene.List(TUserRoleGqlInputObject,required=True)
        
    res = graphene.List(TUserRoleGqlObject)

    def mutate(self, info, inTUserRoleGqlObjects):
        
        objService = TUserRoleService()
        
        return AddTUserRoleGqlObject(
            res = objService.add(inTUserRoleGqlObjects)
        )
class UpdateTUserRoleGqlObject(graphene.Mutation):
    class Arguments:
        inTUserRoleGqlObjects = graphene.List(TUserRoleGqlInputObject,required=True)
        
    res = graphene.List(TUserRoleGqlObject)

    def mutate(self, info, inTUserRoleGqlObjects):
        
        objService = TUserRoleService()
        
        return UpdateTUserRoleGqlObject(
            res = objService.update(inTUserRoleGqlObjects)
        )
class DeleteTUserRoleGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TUserRoleGqlObject)

    def mutate(self, info, f_ids):
        objService = TUserRoleService()
        return DeleteTUserRoleGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTUserRole = DeleteTUserRoleGqlObject.Field()

    addTUserRole = AddTUserRoleGqlObject.Field()

    updateTUserRole = UpdateTUserRoleGqlObject.Field()

    replaceTUserRole = ReplaceTUserRoleGqlObject.Field()

