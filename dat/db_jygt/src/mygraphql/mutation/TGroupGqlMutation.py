#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TGroupGqlObject import TGroupGqlObject,PaginatedTGroupGqlObject,TGroupGqlInputObject
from service.TGroupService import TGroupService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTGroupGqlObject(graphene.Mutation):
    class Arguments:
        inTGroupGqlObjects = graphene.List(TGroupGqlInputObject,required=True)
        
    res = graphene.List(TGroupGqlObject)

    def mutate(self, info, inTGroupGqlObjects):
        
        objService = TGroupService()
        
        return ReplaceTGroupGqlObject(
            res = objService.replace(inTGroupGqlObjects)
        )
class AddTGroupGqlObject(graphene.Mutation):
    class Arguments:
        inTGroupGqlObjects = graphene.List(TGroupGqlInputObject,required=True)
        
    res = graphene.List(TGroupGqlObject)

    def mutate(self, info, inTGroupGqlObjects):
        
        objService = TGroupService()
        
        return AddTGroupGqlObject(
            res = objService.add(inTGroupGqlObjects)
        )
class UpdateTGroupGqlObject(graphene.Mutation):
    class Arguments:
        inTGroupGqlObjects = graphene.List(TGroupGqlInputObject,required=True)
        
    res = graphene.List(TGroupGqlObject)

    def mutate(self, info, inTGroupGqlObjects):
        
        objService = TGroupService()
        
        return UpdateTGroupGqlObject(
            res = objService.update(inTGroupGqlObjects)
        )
class DeleteTGroupGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TGroupGqlObject)

    def mutate(self, info, f_ids):
        objService = TGroupService()
        return DeleteTGroupGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTGroup = DeleteTGroupGqlObject.Field()

    addTGroup = AddTGroupGqlObject.Field()

    updateTGroup = UpdateTGroupGqlObject.Field()

    replaceTGroup = ReplaceTGroupGqlObject.Field()

