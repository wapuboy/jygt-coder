#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TFieldGqlObject import TFieldGqlObject,PaginatedTFieldGqlObject,TFieldGqlInputObject
from service.TFieldService import TFieldService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTFieldGqlObject(graphene.Mutation):
    class Arguments:
        inTFieldGqlObjects = graphene.List(TFieldGqlInputObject,required=True)
        
    res = graphene.List(TFieldGqlObject)

    def mutate(self, info, inTFieldGqlObjects):
        
        objService = TFieldService()
        
        return ReplaceTFieldGqlObject(
            res = objService.replace(inTFieldGqlObjects)
        )
class AddTFieldGqlObject(graphene.Mutation):
    class Arguments:
        inTFieldGqlObjects = graphene.List(TFieldGqlInputObject,required=True)
        
    res = graphene.List(TFieldGqlObject)

    def mutate(self, info, inTFieldGqlObjects):
        
        objService = TFieldService()
        
        return AddTFieldGqlObject(
            res = objService.add(inTFieldGqlObjects)
        )
class UpdateTFieldGqlObject(graphene.Mutation):
    class Arguments:
        inTFieldGqlObjects = graphene.List(TFieldGqlInputObject,required=True)
        
    res = graphene.List(TFieldGqlObject)

    def mutate(self, info, inTFieldGqlObjects):
        
        objService = TFieldService()
        
        return UpdateTFieldGqlObject(
            res = objService.update(inTFieldGqlObjects)
        )
class DeleteTFieldGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TFieldGqlObject)

    def mutate(self, info, f_ids):
        objService = TFieldService()
        return DeleteTFieldGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTField = DeleteTFieldGqlObject.Field()

    addTField = AddTFieldGqlObject.Field()

    updateTField = UpdateTFieldGqlObject.Field()

    replaceTField = ReplaceTFieldGqlObject.Field()

