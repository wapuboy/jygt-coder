#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TRelationRelationGqlObject import TRelationRelationGqlObject,PaginatedTRelationRelationGqlObject,TRelationRelationGqlInputObject
from service.TRelationRelationService import TRelationRelationService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTRelationRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTRelationRelationGqlObjects = graphene.List(TRelationRelationGqlInputObject,required=True)
        
    res = graphene.List(TRelationRelationGqlObject)

    def mutate(self, info, inTRelationRelationGqlObjects):
        
        objService = TRelationRelationService()
        
        return ReplaceTRelationRelationGqlObject(
            res = objService.replace(inTRelationRelationGqlObjects)
        )
class AddTRelationRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTRelationRelationGqlObjects = graphene.List(TRelationRelationGqlInputObject,required=True)
        
    res = graphene.List(TRelationRelationGqlObject)

    def mutate(self, info, inTRelationRelationGqlObjects):
        
        objService = TRelationRelationService()
        
        return AddTRelationRelationGqlObject(
            res = objService.add(inTRelationRelationGqlObjects)
        )
class UpdateTRelationRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTRelationRelationGqlObjects = graphene.List(TRelationRelationGqlInputObject,required=True)
        
    res = graphene.List(TRelationRelationGqlObject)

    def mutate(self, info, inTRelationRelationGqlObjects):
        
        objService = TRelationRelationService()
        
        return UpdateTRelationRelationGqlObject(
            res = objService.update(inTRelationRelationGqlObjects)
        )
class DeleteTRelationRelationGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TRelationRelationGqlObject)

    def mutate(self, info, f_ids):
        objService = TRelationRelationService()
        return DeleteTRelationRelationGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTRelationRelation = DeleteTRelationRelationGqlObject.Field()

    addTRelationRelation = AddTRelationRelationGqlObject.Field()

    updateTRelationRelation = UpdateTRelationRelationGqlObject.Field()

    replaceTRelationRelation = ReplaceTRelationRelationGqlObject.Field()

