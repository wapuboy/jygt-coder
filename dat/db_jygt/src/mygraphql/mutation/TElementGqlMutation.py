#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TElementGqlObject import TElementGqlObject,PaginatedTElementGqlObject,TElementGqlInputObject
from service.TElementService import TElementService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTElementGqlObject(graphene.Mutation):
    class Arguments:
        inTElementGqlObjects = graphene.List(TElementGqlInputObject,required=True)
        
    res = graphene.List(TElementGqlObject)

    def mutate(self, info, inTElementGqlObjects):
        
        objService = TElementService()
        
        return ReplaceTElementGqlObject(
            res = objService.replace(inTElementGqlObjects)
        )
class AddTElementGqlObject(graphene.Mutation):
    class Arguments:
        inTElementGqlObjects = graphene.List(TElementGqlInputObject,required=True)
        
    res = graphene.List(TElementGqlObject)

    def mutate(self, info, inTElementGqlObjects):
        
        objService = TElementService()
        
        return AddTElementGqlObject(
            res = objService.add(inTElementGqlObjects)
        )
class UpdateTElementGqlObject(graphene.Mutation):
    class Arguments:
        inTElementGqlObjects = graphene.List(TElementGqlInputObject,required=True)
        
    res = graphene.List(TElementGqlObject)

    def mutate(self, info, inTElementGqlObjects):
        
        objService = TElementService()
        
        return UpdateTElementGqlObject(
            res = objService.update(inTElementGqlObjects)
        )
class DeleteTElementGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TElementGqlObject)

    def mutate(self, info, f_ids):
        objService = TElementService()
        return DeleteTElementGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTElement = DeleteTElementGqlObject.Field()

    addTElement = AddTElementGqlObject.Field()

    updateTElement = UpdateTElementGqlObject.Field()

    replaceTElement = ReplaceTElementGqlObject.Field()

