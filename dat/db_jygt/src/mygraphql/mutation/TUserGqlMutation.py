#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserGqlObject import TUserGqlObject,PaginatedTUserGqlObject,TUserGqlInputObject
from service.TUserService import TUserService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTUserGqlObject(graphene.Mutation):
    class Arguments:
        inTUserGqlObjects = graphene.List(TUserGqlInputObject,required=True)
        
    res = graphene.List(TUserGqlObject)

    def mutate(self, info, inTUserGqlObjects):
        
        objService = TUserService()
        
        return ReplaceTUserGqlObject(
            res = objService.replace(inTUserGqlObjects)
        )
class AddTUserGqlObject(graphene.Mutation):
    class Arguments:
        inTUserGqlObjects = graphene.List(TUserGqlInputObject,required=True)
        
    res = graphene.List(TUserGqlObject)

    def mutate(self, info, inTUserGqlObjects):
        
        objService = TUserService()
        
        return AddTUserGqlObject(
            res = objService.add(inTUserGqlObjects)
        )
class UpdateTUserGqlObject(graphene.Mutation):
    class Arguments:
        inTUserGqlObjects = graphene.List(TUserGqlInputObject,required=True)
        
    res = graphene.List(TUserGqlObject)

    def mutate(self, info, inTUserGqlObjects):
        
        objService = TUserService()
        
        return UpdateTUserGqlObject(
            res = objService.update(inTUserGqlObjects)
        )
class DeleteTUserGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TUserGqlObject)

    def mutate(self, info, f_ids):
        objService = TUserService()
        return DeleteTUserGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTUser = DeleteTUserGqlObject.Field()

    addTUser = AddTUserGqlObject.Field()

    updateTUser = UpdateTUserGqlObject.Field()

    replaceTUser = ReplaceTUserGqlObject.Field()

