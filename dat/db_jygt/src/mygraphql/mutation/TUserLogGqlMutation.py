#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserLogGqlObject import TUserLogGqlObject,PaginatedTUserLogGqlObject,TUserLogGqlInputObject
from service.TUserLogService import TUserLogService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTUserLogGqlObject(graphene.Mutation):
    class Arguments:
        inTUserLogGqlObjects = graphene.List(TUserLogGqlInputObject,required=True)
        
    res = graphene.List(TUserLogGqlObject)

    def mutate(self, info, inTUserLogGqlObjects):
        
        objService = TUserLogService()
        
        return ReplaceTUserLogGqlObject(
            res = objService.replace(inTUserLogGqlObjects)
        )
class AddTUserLogGqlObject(graphene.Mutation):
    class Arguments:
        inTUserLogGqlObjects = graphene.List(TUserLogGqlInputObject,required=True)
        
    res = graphene.List(TUserLogGqlObject)

    def mutate(self, info, inTUserLogGqlObjects):
        
        objService = TUserLogService()
        
        return AddTUserLogGqlObject(
            res = objService.add(inTUserLogGqlObjects)
        )
class UpdateTUserLogGqlObject(graphene.Mutation):
    class Arguments:
        inTUserLogGqlObjects = graphene.List(TUserLogGqlInputObject,required=True)
        
    res = graphene.List(TUserLogGqlObject)

    def mutate(self, info, inTUserLogGqlObjects):
        
        objService = TUserLogService()
        
        return UpdateTUserLogGqlObject(
            res = objService.update(inTUserLogGqlObjects)
        )
class DeleteTUserLogGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TUserLogGqlObject)

    def mutate(self, info, f_ids):
        objService = TUserLogService()
        return DeleteTUserLogGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTUserLog = DeleteTUserLogGqlObject.Field()

    addTUserLog = AddTUserLogGqlObject.Field()

    updateTUserLog = UpdateTUserLogGqlObject.Field()

    replaceTUserLog = ReplaceTUserLogGqlObject.Field()

