#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TPageGqlObject import TPageGqlObject,PaginatedTPageGqlObject,TPageGqlInputObject
from service.TPageService import TPageService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTPageGqlObject(graphene.Mutation):
    class Arguments:
        inTPageGqlObjects = graphene.List(TPageGqlInputObject,required=True)
        
    res = graphene.List(TPageGqlObject)

    def mutate(self, info, inTPageGqlObjects):
        
        objService = TPageService()
        
        return ReplaceTPageGqlObject(
            res = objService.replace(inTPageGqlObjects)
        )
class AddTPageGqlObject(graphene.Mutation):
    class Arguments:
        inTPageGqlObjects = graphene.List(TPageGqlInputObject,required=True)
        
    res = graphene.List(TPageGqlObject)

    def mutate(self, info, inTPageGqlObjects):
        
        objService = TPageService()
        
        return AddTPageGqlObject(
            res = objService.add(inTPageGqlObjects)
        )
class UpdateTPageGqlObject(graphene.Mutation):
    class Arguments:
        inTPageGqlObjects = graphene.List(TPageGqlInputObject,required=True)
        
    res = graphene.List(TPageGqlObject)

    def mutate(self, info, inTPageGqlObjects):
        
        objService = TPageService()
        
        return UpdateTPageGqlObject(
            res = objService.update(inTPageGqlObjects)
        )
class DeleteTPageGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TPageGqlObject)

    def mutate(self, info, f_ids):
        objService = TPageService()
        return DeleteTPageGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTPage = DeleteTPageGqlObject.Field()

    addTPage = AddTPageGqlObject.Field()

    updateTPage = UpdateTPageGqlObject.Field()

    replaceTPage = ReplaceTPageGqlObject.Field()

