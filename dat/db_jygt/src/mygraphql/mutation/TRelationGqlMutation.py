#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TRelationGqlObject import TRelationGqlObject,PaginatedTRelationGqlObject,TRelationGqlInputObject
from service.TRelationService import TRelationService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTRelationGqlObjects = graphene.List(TRelationGqlInputObject,required=True)
        
    res = graphene.List(TRelationGqlObject)

    def mutate(self, info, inTRelationGqlObjects):
        
        objService = TRelationService()
        
        return ReplaceTRelationGqlObject(
            res = objService.replace(inTRelationGqlObjects)
        )
class AddTRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTRelationGqlObjects = graphene.List(TRelationGqlInputObject,required=True)
        
    res = graphene.List(TRelationGqlObject)

    def mutate(self, info, inTRelationGqlObjects):
        
        objService = TRelationService()
        
        return AddTRelationGqlObject(
            res = objService.add(inTRelationGqlObjects)
        )
class UpdateTRelationGqlObject(graphene.Mutation):
    class Arguments:
        inTRelationGqlObjects = graphene.List(TRelationGqlInputObject,required=True)
        
    res = graphene.List(TRelationGqlObject)

    def mutate(self, info, inTRelationGqlObjects):
        
        objService = TRelationService()
        
        return UpdateTRelationGqlObject(
            res = objService.update(inTRelationGqlObjects)
        )
class DeleteTRelationGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TRelationGqlObject)

    def mutate(self, info, f_ids):
        objService = TRelationService()
        return DeleteTRelationGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTRelation = DeleteTRelationGqlObject.Field()

    addTRelation = AddTRelationGqlObject.Field()

    updateTRelation = UpdateTRelationGqlObject.Field()

    replaceTRelation = ReplaceTRelationGqlObject.Field()

