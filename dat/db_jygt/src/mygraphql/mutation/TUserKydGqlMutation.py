#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserKydGqlObject import TUserKydGqlObject,PaginatedTUserKydGqlObject,TUserKydGqlInputObject
from service.TUserKydService import TUserKydService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTUserKydGqlObject(graphene.Mutation):
    class Arguments:
        inTUserKydGqlObjects = graphene.List(TUserKydGqlInputObject,required=True)
        
    res = graphene.List(TUserKydGqlObject)

    def mutate(self, info, inTUserKydGqlObjects):
        
        objService = TUserKydService()
        
        return ReplaceTUserKydGqlObject(
            res = objService.replace(inTUserKydGqlObjects)
        )
class AddTUserKydGqlObject(graphene.Mutation):
    class Arguments:
        inTUserKydGqlObjects = graphene.List(TUserKydGqlInputObject,required=True)
        
    res = graphene.List(TUserKydGqlObject)

    def mutate(self, info, inTUserKydGqlObjects):
        
        objService = TUserKydService()
        
        return AddTUserKydGqlObject(
            res = objService.add(inTUserKydGqlObjects)
        )
class UpdateTUserKydGqlObject(graphene.Mutation):
    class Arguments:
        inTUserKydGqlObjects = graphene.List(TUserKydGqlInputObject,required=True)
        
    res = graphene.List(TUserKydGqlObject)

    def mutate(self, info, inTUserKydGqlObjects):
        
        objService = TUserKydService()
        
        return UpdateTUserKydGqlObject(
            res = objService.update(inTUserKydGqlObjects)
        )
class DeleteTUserKydGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TUserKydGqlObject)

    def mutate(self, info, f_ids):
        objService = TUserKydService()
        return DeleteTUserKydGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTUserKyd = DeleteTUserKydGqlObject.Field()

    addTUserKyd = AddTUserKydGqlObject.Field()

    updateTUserKyd = UpdateTUserKydGqlObject.Field()

    replaceTUserKyd = ReplaceTUserKydGqlObject.Field()

