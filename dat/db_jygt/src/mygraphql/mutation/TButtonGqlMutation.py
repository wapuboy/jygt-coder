#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TButtonGqlObject import TButtonGqlObject,PaginatedTButtonGqlObject,TButtonGqlInputObject
from service.TButtonService import TButtonService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTButtonGqlObject(graphene.Mutation):
    class Arguments:
        inTButtonGqlObjects = graphene.List(TButtonGqlInputObject,required=True)
        
    res = graphene.List(TButtonGqlObject)

    def mutate(self, info, inTButtonGqlObjects):
        
        objService = TButtonService()
        
        return ReplaceTButtonGqlObject(
            res = objService.replace(inTButtonGqlObjects)
        )
class AddTButtonGqlObject(graphene.Mutation):
    class Arguments:
        inTButtonGqlObjects = graphene.List(TButtonGqlInputObject,required=True)
        
    res = graphene.List(TButtonGqlObject)

    def mutate(self, info, inTButtonGqlObjects):
        
        objService = TButtonService()
        
        return AddTButtonGqlObject(
            res = objService.add(inTButtonGqlObjects)
        )
class UpdateTButtonGqlObject(graphene.Mutation):
    class Arguments:
        inTButtonGqlObjects = graphene.List(TButtonGqlInputObject,required=True)
        
    res = graphene.List(TButtonGqlObject)

    def mutate(self, info, inTButtonGqlObjects):
        
        objService = TButtonService()
        
        return UpdateTButtonGqlObject(
            res = objService.update(inTButtonGqlObjects)
        )
class DeleteTButtonGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TButtonGqlObject)

    def mutate(self, info, f_ids):
        objService = TButtonService()
        return DeleteTButtonGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTButton = DeleteTButtonGqlObject.Field()

    addTButton = AddTButtonGqlObject.Field()

    updateTButton = UpdateTButtonGqlObject.Field()

    replaceTButton = ReplaceTButtonGqlObject.Field()

