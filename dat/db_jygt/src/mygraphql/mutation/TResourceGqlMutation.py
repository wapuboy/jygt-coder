#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TResourceGqlObject import TResourceGqlObject,PaginatedTResourceGqlObject,TResourceGqlInputObject
from service.TResourceService import TResourceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTResourceGqlObject(graphene.Mutation):
    class Arguments:
        inTResourceGqlObjects = graphene.List(TResourceGqlInputObject,required=True)
        
    res = graphene.List(TResourceGqlObject)

    def mutate(self, info, inTResourceGqlObjects):
        
        objService = TResourceService()
        
        return ReplaceTResourceGqlObject(
            res = objService.replace(inTResourceGqlObjects)
        )
class AddTResourceGqlObject(graphene.Mutation):
    class Arguments:
        inTResourceGqlObjects = graphene.List(TResourceGqlInputObject,required=True)
        
    res = graphene.List(TResourceGqlObject)

    def mutate(self, info, inTResourceGqlObjects):
        
        objService = TResourceService()
        
        return AddTResourceGqlObject(
            res = objService.add(inTResourceGqlObjects)
        )
class UpdateTResourceGqlObject(graphene.Mutation):
    class Arguments:
        inTResourceGqlObjects = graphene.List(TResourceGqlInputObject,required=True)
        
    res = graphene.List(TResourceGqlObject)

    def mutate(self, info, inTResourceGqlObjects):
        
        objService = TResourceService()
        
        return UpdateTResourceGqlObject(
            res = objService.update(inTResourceGqlObjects)
        )
class DeleteTResourceGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TResourceGqlObject)

    def mutate(self, info, f_ids):
        objService = TResourceService()
        return DeleteTResourceGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTResource = DeleteTResourceGqlObject.Field()

    addTResource = AddTResourceGqlObject.Field()

    updateTResource = UpdateTResourceGqlObject.Field()

    replaceTResource = ReplaceTResourceGqlObject.Field()

