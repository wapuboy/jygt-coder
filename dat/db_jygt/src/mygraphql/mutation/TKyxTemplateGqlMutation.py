#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TKyxTemplateGqlObject import TKyxTemplateGqlObject,PaginatedTKyxTemplateGqlObject,TKyxTemplateGqlInputObject
from service.TKyxTemplateService import TKyxTemplateService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTKyxTemplateGqlObject(graphene.Mutation):
    class Arguments:
        inTKyxTemplateGqlObjects = graphene.List(TKyxTemplateGqlInputObject,required=True)
        
    res = graphene.List(TKyxTemplateGqlObject)

    def mutate(self, info, inTKyxTemplateGqlObjects):
        
        objService = TKyxTemplateService()
        
        return ReplaceTKyxTemplateGqlObject(
            res = objService.replace(inTKyxTemplateGqlObjects)
        )
class AddTKyxTemplateGqlObject(graphene.Mutation):
    class Arguments:
        inTKyxTemplateGqlObjects = graphene.List(TKyxTemplateGqlInputObject,required=True)
        
    res = graphene.List(TKyxTemplateGqlObject)

    def mutate(self, info, inTKyxTemplateGqlObjects):
        
        objService = TKyxTemplateService()
        
        return AddTKyxTemplateGqlObject(
            res = objService.add(inTKyxTemplateGqlObjects)
        )
class UpdateTKyxTemplateGqlObject(graphene.Mutation):
    class Arguments:
        inTKyxTemplateGqlObjects = graphene.List(TKyxTemplateGqlInputObject,required=True)
        
    res = graphene.List(TKyxTemplateGqlObject)

    def mutate(self, info, inTKyxTemplateGqlObjects):
        
        objService = TKyxTemplateService()
        
        return UpdateTKyxTemplateGqlObject(
            res = objService.update(inTKyxTemplateGqlObjects)
        )
class DeleteTKyxTemplateGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TKyxTemplateGqlObject)

    def mutate(self, info, f_ids):
        objService = TKyxTemplateService()
        return DeleteTKyxTemplateGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTKyxTemplate = DeleteTKyxTemplateGqlObject.Field()

    addTKyxTemplate = AddTKyxTemplateGqlObject.Field()

    updateTKyxTemplate = UpdateTKyxTemplateGqlObject.Field()

    replaceTKyxTemplate = ReplaceTKyxTemplateGqlObject.Field()

