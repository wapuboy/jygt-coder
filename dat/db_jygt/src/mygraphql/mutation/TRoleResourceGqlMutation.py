#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TRoleResourceGqlObject import TRoleResourceGqlObject,PaginatedTRoleResourceGqlObject,TRoleResourceGqlInputObject
from service.TRoleResourceService import TRoleResourceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTRoleResourceGqlObject(graphene.Mutation):
    class Arguments:
        inTRoleResourceGqlObjects = graphene.List(TRoleResourceGqlInputObject,required=True)
        
    res = graphene.List(TRoleResourceGqlObject)

    def mutate(self, info, inTRoleResourceGqlObjects):
        
        objService = TRoleResourceService()
        
        return ReplaceTRoleResourceGqlObject(
            res = objService.replace(inTRoleResourceGqlObjects)
        )
class AddTRoleResourceGqlObject(graphene.Mutation):
    class Arguments:
        inTRoleResourceGqlObjects = graphene.List(TRoleResourceGqlInputObject,required=True)
        
    res = graphene.List(TRoleResourceGqlObject)

    def mutate(self, info, inTRoleResourceGqlObjects):
        
        objService = TRoleResourceService()
        
        return AddTRoleResourceGqlObject(
            res = objService.add(inTRoleResourceGqlObjects)
        )
class UpdateTRoleResourceGqlObject(graphene.Mutation):
    class Arguments:
        inTRoleResourceGqlObjects = graphene.List(TRoleResourceGqlInputObject,required=True)
        
    res = graphene.List(TRoleResourceGqlObject)

    def mutate(self, info, inTRoleResourceGqlObjects):
        
        objService = TRoleResourceService()
        
        return UpdateTRoleResourceGqlObject(
            res = objService.update(inTRoleResourceGqlObjects)
        )
class DeleteTRoleResourceGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TRoleResourceGqlObject)

    def mutate(self, info, f_ids):
        objService = TRoleResourceService()
        return DeleteTRoleResourceGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTRoleResource = DeleteTRoleResourceGqlObject.Field()

    addTRoleResource = AddTRoleResourceGqlObject.Field()

    updateTRoleResource = UpdateTRoleResourceGqlObject.Field()

    replaceTRoleResource = ReplaceTRoleResourceGqlObject.Field()

