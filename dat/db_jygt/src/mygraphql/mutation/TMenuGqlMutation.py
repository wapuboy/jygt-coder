#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TMenuGqlObject import TMenuGqlObject,PaginatedTMenuGqlObject,TMenuGqlInputObject
from service.TMenuService import TMenuService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTMenuGqlObject(graphene.Mutation):
    class Arguments:
        inTMenuGqlObjects = graphene.List(TMenuGqlInputObject,required=True)
        
    res = graphene.List(TMenuGqlObject)

    def mutate(self, info, inTMenuGqlObjects):
        
        objService = TMenuService()
        
        return ReplaceTMenuGqlObject(
            res = objService.replace(inTMenuGqlObjects)
        )
class AddTMenuGqlObject(graphene.Mutation):
    class Arguments:
        inTMenuGqlObjects = graphene.List(TMenuGqlInputObject,required=True)
        
    res = graphene.List(TMenuGqlObject)

    def mutate(self, info, inTMenuGqlObjects):
        
        objService = TMenuService()
        
        return AddTMenuGqlObject(
            res = objService.add(inTMenuGqlObjects)
        )
class UpdateTMenuGqlObject(graphene.Mutation):
    class Arguments:
        inTMenuGqlObjects = graphene.List(TMenuGqlInputObject,required=True)
        
    res = graphene.List(TMenuGqlObject)

    def mutate(self, info, inTMenuGqlObjects):
        
        objService = TMenuService()
        
        return UpdateTMenuGqlObject(
            res = objService.update(inTMenuGqlObjects)
        )
class DeleteTMenuGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TMenuGqlObject)

    def mutate(self, info, f_ids):
        objService = TMenuService()
        return DeleteTMenuGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTMenu = DeleteTMenuGqlObject.Field()

    addTMenu = AddTMenuGqlObject.Field()

    updateTMenu = UpdateTMenuGqlObject.Field()

    replaceTMenu = ReplaceTMenuGqlObject.Field()

