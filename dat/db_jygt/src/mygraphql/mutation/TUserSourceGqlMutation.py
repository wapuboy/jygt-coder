#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene
from mygraphql.object.TUserSourceGqlObject import TUserSourceGqlObject,PaginatedTUserSourceGqlObject,TUserSourceGqlInputObject
from service.TUserSourceService import TUserSourceService

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ReplaceTUserSourceGqlObject(graphene.Mutation):
    class Arguments:
        inTUserSourceGqlObjects = graphene.List(TUserSourceGqlInputObject,required=True)
        
    res = graphene.List(TUserSourceGqlObject)

    def mutate(self, info, inTUserSourceGqlObjects):
        
        objService = TUserSourceService()
        
        return ReplaceTUserSourceGqlObject(
            res = objService.replace(inTUserSourceGqlObjects)
        )
class AddTUserSourceGqlObject(graphene.Mutation):
    class Arguments:
        inTUserSourceGqlObjects = graphene.List(TUserSourceGqlInputObject,required=True)
        
    res = graphene.List(TUserSourceGqlObject)

    def mutate(self, info, inTUserSourceGqlObjects):
        
        objService = TUserSourceService()
        
        return AddTUserSourceGqlObject(
            res = objService.add(inTUserSourceGqlObjects)
        )
class UpdateTUserSourceGqlObject(graphene.Mutation):
    class Arguments:
        inTUserSourceGqlObjects = graphene.List(TUserSourceGqlInputObject,required=True)
        
    res = graphene.List(TUserSourceGqlObject)

    def mutate(self, info, inTUserSourceGqlObjects):
        
        objService = TUserSourceService()
        
        return UpdateTUserSourceGqlObject(
            res = objService.update(inTUserSourceGqlObjects)
        )
class DeleteTUserSourceGqlObject(graphene.Mutation):
    class Arguments:
        f_ids = graphene.List(graphene.Int,required=True)
       
    res = graphene.List(TUserSourceGqlObject)

    def mutate(self, info, f_ids):
        objService = TUserSourceService()
        return DeleteTUserSourceGqlObject(
            res = objService.delete(f_ids)
        ) 

class Mutation(graphene.ObjectType):
    deleteTUserSource = DeleteTUserSourceGqlObject.Field()

    addTUserSource = AddTUserSourceGqlObject.Field()

    updateTUserSource = UpdateTUserSourceGqlObject.Field()

    replaceTUserSource = ReplaceTUserSourceGqlObject.Field()

