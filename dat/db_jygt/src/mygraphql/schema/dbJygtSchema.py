#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
import graphene



    
from mygraphql.mutation import TButtonGqlMutation
from mygraphql.query.TButtonGqlQuery import TButtonGqlQuery
    
    
    
from mygraphql.mutation import TElementGqlMutation
from mygraphql.query.TElementGqlQuery import TElementGqlQuery
    
    
    
from mygraphql.mutation import TFieldGqlMutation
from mygraphql.query.TFieldGqlQuery import TFieldGqlQuery
    
    
    
from mygraphql.mutation import TFieldContentGqlMutation
from mygraphql.query.TFieldContentGqlQuery import TFieldContentGqlQuery
    
    
    
from mygraphql.mutation import TGroupGqlMutation
from mygraphql.query.TGroupGqlQuery import TGroupGqlQuery
    
    
    
from mygraphql.mutation import TInterfaceGqlMutation
from mygraphql.query.TInterfaceGqlQuery import TInterfaceGqlQuery
    
    
    
from mygraphql.mutation import TKyxTemplateGqlMutation
from mygraphql.query.TKyxTemplateGqlQuery import TKyxTemplateGqlQuery
    
    
    
from mygraphql.mutation import TMenuGqlMutation
from mygraphql.query.TMenuGqlQuery import TMenuGqlQuery
    
    
    
from mygraphql.mutation import TPageGqlMutation
from mygraphql.query.TPageGqlQuery import TPageGqlQuery
    
    
    
from mygraphql.mutation import TRelationGqlMutation
from mygraphql.query.TRelationGqlQuery import TRelationGqlQuery
    
    
    
from mygraphql.mutation import TRelationRelationGqlMutation
from mygraphql.query.TRelationRelationGqlQuery import TRelationRelationGqlQuery
    
    
    
from mygraphql.mutation import TResourceGqlMutation
from mygraphql.query.TResourceGqlQuery import TResourceGqlQuery
    
    
    
from mygraphql.mutation import TRoleGqlMutation
from mygraphql.query.TRoleGqlQuery import TRoleGqlQuery
    
    
    
from mygraphql.mutation import TRoleResourceGqlMutation
from mygraphql.query.TRoleResourceGqlQuery import TRoleResourceGqlQuery
    
    
    
from mygraphql.mutation import TUserGqlMutation
from mygraphql.query.TUserGqlQuery import TUserGqlQuery
    
    
    
from mygraphql.mutation import TUserKydGqlMutation
from mygraphql.query.TUserKydGqlQuery import TUserKydGqlQuery
    
    
    
from mygraphql.mutation import TUserLogGqlMutation
from mygraphql.query.TUserLogGqlQuery import TUserLogGqlQuery
    
    
    
from mygraphql.mutation import TUserRelationGqlMutation
from mygraphql.query.TUserRelationGqlQuery import TUserRelationGqlQuery
    
    
    
from mygraphql.mutation import TUserRoleGqlMutation
from mygraphql.query.TUserRoleGqlQuery import TUserRoleGqlQuery
    
    
    
from mygraphql.mutation import TUserSourceGqlMutation
from mygraphql.query.TUserSourceGqlQuery import TUserSourceGqlQuery
    
    

class AllQuery(TButtonGqlQuery,TElementGqlQuery,TFieldGqlQuery,TFieldContentGqlQuery,TGroupGqlQuery,TInterfaceGqlQuery,TKyxTemplateGqlQuery,TMenuGqlQuery,TPageGqlQuery,TRelationGqlQuery,TRelationRelationGqlQuery,TResourceGqlQuery,TRoleGqlQuery,TRoleResourceGqlQuery,TUserGqlQuery,TUserKydGqlQuery,TUserLogGqlQuery,TUserRelationGqlQuery,TUserRoleGqlQuery,TUserSourceGqlQuery
            ,graphene.ObjectType):
  pass


class AllMutation(TButtonGqlMutation.Mutation,TElementGqlMutation.Mutation,TFieldGqlMutation.Mutation,TFieldContentGqlMutation.Mutation,TGroupGqlMutation.Mutation,TInterfaceGqlMutation.Mutation,TKyxTemplateGqlMutation.Mutation,TMenuGqlMutation.Mutation,TPageGqlMutation.Mutation,TRelationGqlMutation.Mutation,TRelationRelationGqlMutation.Mutation,TResourceGqlMutation.Mutation,TRoleGqlMutation.Mutation,TRoleResourceGqlMutation.Mutation,TUserGqlMutation.Mutation,TUserKydGqlMutation.Mutation,TUserLogGqlMutation.Mutation,TUserRelationGqlMutation.Mutation,TUserRoleGqlMutation.Mutation,TUserSourceGqlMutation.Mutation
                  ,graphene.ObjectType):
  pass

dbJygtSchema = graphene.Schema(query=AllQuery,mutation=AllMutation)


