#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
from connection.MydbPool import mydb_pool_session, mydb_transaction, mydb_copy
from entity.TUserRelationEntity import TUserRelationEntity

import re
from sqlalchemy import or_, and_
from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TUserRelationService:

    @mydb_pool_session
    def getAll(self, page=0, per_page=10):
        """
        获取所有 TUserRelationService 记录，支持分页。

        :param page: 当前页码，默认为 1。
        :param per_page: 每页记录数，默认为 10。
        :return: 包含总记录数、总页数、当前页码和记录列表的字典。
        """
        try:
            logger.info(f"{self.__class__.__name__}.getAll")
            items = self.session.query(TUserRelationEntity
                ).limit(per_page).offset(page * per_page).all()

            total = self.session.query(TUserRelationEntity).count()

            pages = (total + per_page - 1) // per_page

            return {
                'total': total,
                'pages': pages,
                'current_page': page,
                'items': items
            }
        except Exception as e:
            logger.error(f"Error fetching all TUserRelationEntity records: {e}")
            raise
    
    @mydb_pool_session
    def _get_by_filters(self, filters: list) -> list:
        """
        根据给定的过滤条件查询 TUserRelationEntity 记录。

        :param filters: 过滤条件列表。
        :return: 查询结果列表。
        """
        try:
            query = self.session.query(TUserRelationEntity).filter(and_(*filters))
            return query.all()
        except Exception as e:
            logger.error(f"Error fetching TUserRelationEntity records with filters {filters}: {e}")
            raise

    @mydb_pool_session
    def getByPrimary(self, f_id):
        """
        根据 表t_user_relation索引 PRIMARY  获取记录。

        输入参数：
                f_id
        
        输出参数：
                TUserRelationEntity对象或 None
        """
        
        logger.info(f"{self.__class__.__name__}.getByPrimary")
        
        filters = [
                TUserRelationEntity.f_id == f_id,
            1==1
        ]

        results = self._get_by_filters(filters)
        return results[0] if results else None
    @mydb_pool_session
    def getByUnqUid1AndRidAndUid2(self, f_user_id_1, f_relation_id, f_user_id_2):
        """
        根据 表t_user_relation索引 unq_uid1_and_rid_and_uid2  获取记录。

        输入参数：
                f_user_id_1
                f_relation_id
                f_user_id_2
        
        输出参数：
                TUserRelationEntity对象或 None
        """
        
        logger.info(f"{self.__class__.__name__}.getByUnqUid1AndRidAndUid2")
        
        filters = [
                TUserRelationEntity.f_user_id_1 == f_user_id_1,
                TUserRelationEntity.f_relation_id == f_relation_id,
                TUserRelationEntity.f_user_id_2 == f_user_id_2,
            1==1
        ]

        results = self._get_by_filters(filters)
        return results[0] if results else None
    @mydb_pool_session
    def getByIdxUid1(self, f_user_id_1):
        """
        根据 表t_user_relation索引 idx_uid1  获取记录。

        输入参数：
                f_user_id_1
        
        输出参数：
                TUserRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxUid1")
        
        filters = [
                TUserRelationEntity.f_user_id_1 == f_user_id_1,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    def getByIdxUid1AndRid(self, f_user_id_1, f_relation_id):
        """
        根据 表t_user_relation索引 idx_uid1_and_rid  获取记录。

        输入参数：
                f_user_id_1
                f_relation_id
        
        输出参数：
                TUserRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxUid1AndRid")
        
        filters = [
                TUserRelationEntity.f_user_id_1 == f_user_id_1,
                TUserRelationEntity.f_relation_id == f_relation_id,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    def getByIdxUid2(self, f_user_id_2):
        """
        根据 表t_user_relation索引 idx_uid2  获取记录。

        输入参数：
                f_user_id_2
        
        输出参数：
                TUserRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxUid2")
        
        filters = [
                TUserRelationEntity.f_user_id_2 == f_user_id_2,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    def getByIdxUid2AndRid(self, f_relation_id, f_user_id_2):
        """
        根据 表t_user_relation索引 idx_uid2_and_rid  获取记录。

        输入参数：
                f_relation_id
                f_user_id_2
        
        输出参数：
                TUserRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxUid2AndRid")
        
        filters = [
                TUserRelationEntity.f_relation_id == f_relation_id,
                TUserRelationEntity.f_user_id_2 == f_user_id_2,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    @mydb_transaction
    def replace(self, records):
        """
        批量替换数据，当数据存在时更新，当数据不存在时增加。

        :param records: 要替换的数据列表。
        :return: 替换后的数据列表。
        """
        if not isinstance(records, list):
            records = [records]
        
        requests = []
        
        for record in records:
            obj = TUserRelationEntity(**record)

            if record['f_id']:
                objOld = self.getByPrimary(f_id=record['f_id'])
                if objOld is None:
                    self.session.add(obj)
                    requests.append(obj)
                else:
                    mydb_copy(obj, objOld)
                    self.session.merge(objOld)
                    requests.append(objOld)
            else:
                self.session.add(obj)
                requests.append(obj)
        return requests
    @mydb_pool_session
    @mydb_transaction
    def add(self, records):
        """
        批量增加数据，当数据不存在时增加。

        :param records: 要增加的数据列表。
        :return: 增加后的数据列表。
        """
        if not isinstance(records, list):
            records = [records]
        
        requests = []
        
        for record in records:
            obj = TUserRelationEntity(**record)

            if record['f_id']:
                objOld = self.getByPrimary(f_id=record['f_id'])
                if objOld is None:
                    self.session.add(obj)
                    requests.append(obj)
            else:
                self.session.add(obj)
                requests.append(obj)
        return requests
    @mydb_pool_session
    @mydb_transaction
    def update(self, records):
        """
        批量修改数据，当数据存在时修改。

        :param records: 要修改的数据列表。
        :return: 修改后的数据列表。
        """
        if not isinstance(records, list):
            records = [records]
        
        requests = []
        
        for record in records:
            obj = TUserRelationEntity(**record)

            if record['f_id']:
                objOld = self.getByPrimary(f_id=record['f_id'])
                if objOld :
                    mydb_copy(obj, objOld)
                    self.session.merge(objOld)
                    requests.append(objOld)            
        return requests
    @mydb_pool_session
    def delete(self, f_ids):
        """
        删除指定主键集合的记录。

        输入参数：
            单值 f_id
            集合 [f_id,...]
        
        输出参数：
            删除的实体对象列表 [TUserRelationEntity]
        """
        if not isinstance(f_ids, list):
            f_ids = [f_ids]
        
        deleted_items = []
        for f_id in f_ids:
            try:
                oldRecord = self.getByPrimary(f_id=f_id)
                if oldRecord is None:
                    continue

                self.session.delete(oldRecord)
                deleted_items.append(oldRecord)
            
            except Exception as exp:
                logger.error(exp)
                self.session.rollback()
        
        self.session.commit()
        return deleted_items
