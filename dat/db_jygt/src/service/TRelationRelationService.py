#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: 2024-12-16 23:03:18                                        
#
from connection.MydbPool import mydb_pool_session, mydb_transaction, mydb_copy
from entity.TRelationRelationEntity import TRelationRelationEntity

import re
from sqlalchemy import or_, and_
from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class TRelationRelationService:

    @mydb_pool_session
    def getAll(self, page=0, per_page=10):
        """
        获取所有 TRelationRelationService 记录，支持分页。

        :param page: 当前页码，默认为 1。
        :param per_page: 每页记录数，默认为 10。
        :return: 包含总记录数、总页数、当前页码和记录列表的字典。
        """
        try:
            logger.info(f"{self.__class__.__name__}.getAll")
            items = self.session.query(TRelationRelationEntity
                ).limit(per_page).offset(page * per_page).all()

            total = self.session.query(TRelationRelationEntity).count()

            pages = (total + per_page - 1) // per_page

            return {
                'total': total,
                'pages': pages,
                'current_page': page,
                'items': items
            }
        except Exception as e:
            logger.error(f"Error fetching all TRelationRelationEntity records: {e}")
            raise
    
    @mydb_pool_session
    def _get_by_filters(self, filters: list) -> list:
        """
        根据给定的过滤条件查询 TRelationRelationEntity 记录。

        :param filters: 过滤条件列表。
        :return: 查询结果列表。
        """
        try:
            query = self.session.query(TRelationRelationEntity).filter(and_(*filters))
            return query.all()
        except Exception as e:
            logger.error(f"Error fetching TRelationRelationEntity records with filters {filters}: {e}")
            raise

    @mydb_pool_session
    def getByPrimary(self, f_id):
        """
        根据 表t_relation_relation索引 PRIMARY  获取记录。

        输入参数：
                f_id
        
        输出参数：
                TRelationRelationEntity对象或 None
        """
        
        logger.info(f"{self.__class__.__name__}.getByPrimary")
        
        filters = [
                TRelationRelationEntity.f_id == f_id,
            1==1
        ]

        results = self._get_by_filters(filters)
        return results[0] if results else None
    @mydb_pool_session
    def getByUnqR1AndTypeAndR2(self, f_relation_id_1, f_type, f_relation_id_2):
        """
        根据 表t_relation_relation索引 unq_r1_and_type_and_r2  获取记录。

        输入参数：
                f_relation_id_1
                f_type
                f_relation_id_2
        
        输出参数：
                TRelationRelationEntity对象或 None
        """
        
        logger.info(f"{self.__class__.__name__}.getByUnqR1AndTypeAndR2")
        
        filters = [
                TRelationRelationEntity.f_relation_id_1 == f_relation_id_1,
                TRelationRelationEntity.f_type == f_type,
                TRelationRelationEntity.f_relation_id_2 == f_relation_id_2,
            1==1
        ]

        results = self._get_by_filters(filters)
        return results[0] if results else None
    @mydb_pool_session
    def getByIdxR2(self, f_relation_id_2):
        """
        根据 表t_relation_relation索引 idx_r2  获取记录。

        输入参数：
                f_relation_id_2
        
        输出参数：
                TRelationRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxR2")
        
        filters = [
                TRelationRelationEntity.f_relation_id_2 == f_relation_id_2,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    def getByIdxR1(self, f_relation_id_1):
        """
        根据 表t_relation_relation索引 idx_r1  获取记录。

        输入参数：
                f_relation_id_1
        
        输出参数：
                TRelationRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxR1")
        
        filters = [
                TRelationRelationEntity.f_relation_id_1 == f_relation_id_1,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    def getByIdxR1AndType(self, f_relation_id_1, f_type):
        """
        根据 表t_relation_relation索引 idx_r1_and_type  获取记录。

        输入参数：
                f_relation_id_1
                f_type
        
        输出参数：
                TRelationRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxR1AndType")
        
        filters = [
                TRelationRelationEntity.f_relation_id_1 == f_relation_id_1,
                TRelationRelationEntity.f_type == f_type,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    def getByIdxTypeAndR2(self, f_type, f_relation_id_2):
        """
        根据 表t_relation_relation索引 idx_type_and_r2  获取记录。

        输入参数：
                f_type
                f_relation_id_2
        
        输出参数：
                TRelationRelationEntity列表或空
        """
        
        logger.info(f"{self.__class__.__name__}.getByIdxTypeAndR2")
        
        filters = [
                TRelationRelationEntity.f_type == f_type,
                TRelationRelationEntity.f_relation_id_2 == f_relation_id_2,
            1==1
        ]

        return self._get_by_filters(filters)
    @mydb_pool_session
    @mydb_transaction
    def replace(self, records):
        """
        批量替换数据，当数据存在时更新，当数据不存在时增加。

        :param records: 要替换的数据列表。
        :return: 替换后的数据列表。
        """
        if not isinstance(records, list):
            records = [records]
        
        requests = []
        
        for record in records:
            obj = TRelationRelationEntity(**record)

            if record['f_id']:
                objOld = self.getByPrimary(f_id=record['f_id'])
                if objOld is None:
                    self.session.add(obj)
                    requests.append(obj)
                else:
                    mydb_copy(obj, objOld)
                    self.session.merge(objOld)
                    requests.append(objOld)
            else:
                self.session.add(obj)
                requests.append(obj)
        return requests
    @mydb_pool_session
    @mydb_transaction
    def add(self, records):
        """
        批量增加数据，当数据不存在时增加。

        :param records: 要增加的数据列表。
        :return: 增加后的数据列表。
        """
        if not isinstance(records, list):
            records = [records]
        
        requests = []
        
        for record in records:
            obj = TRelationRelationEntity(**record)

            if record['f_id']:
                objOld = self.getByPrimary(f_id=record['f_id'])
                if objOld is None:
                    self.session.add(obj)
                    requests.append(obj)
            else:
                self.session.add(obj)
                requests.append(obj)
        return requests
    @mydb_pool_session
    @mydb_transaction
    def update(self, records):
        """
        批量修改数据，当数据存在时修改。

        :param records: 要修改的数据列表。
        :return: 修改后的数据列表。
        """
        if not isinstance(records, list):
            records = [records]
        
        requests = []
        
        for record in records:
            obj = TRelationRelationEntity(**record)

            if record['f_id']:
                objOld = self.getByPrimary(f_id=record['f_id'])
                if objOld :
                    mydb_copy(obj, objOld)
                    self.session.merge(objOld)
                    requests.append(objOld)            
        return requests
    @mydb_pool_session
    def delete(self, f_ids):
        """
        删除指定主键集合的记录。

        输入参数：
            单值 f_id
            集合 [f_id,...]
        
        输出参数：
            删除的实体对象列表 [TRelationRelationEntity]
        """
        if not isinstance(f_ids, list):
            f_ids = [f_ids]
        
        deleted_items = []
        for f_id in f_ids:
            try:
                oldRecord = self.getByPrimary(f_id=f_id)
                if oldRecord is None:
                    continue

                self.session.delete(oldRecord)
                deleted_items.append(oldRecord)
            
            except Exception as exp:
                logger.error(exp)
                self.session.rollback()
        
        self.session.commit()
        return deleted_items
