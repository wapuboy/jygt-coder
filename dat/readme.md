## dat
dat目录默认存放生成的WebAPI工程代码。使用JYGT-CODER生成的WebAPI工程，将存放在data\dist\目录下。

### db_jygt

是系统的示例工程，使用JYGT-CODER生成。使用此工程代码，需要在本地安装一个MySQL数据库，并创建一个名为db_jygt的数据库，然后执行[db_jygt.sql](../src/db/mysql/db_jygt.sql)文件，完成数据库的初始化。如下参数：
- host : 127.0.0.1
- user : root
- password : root1234
- database : db_jygt
- port : 3306

## dist
dist目录存放的是JYGT-CODER生成的WebAPI工程。