## 代码来源
此代码由[JYGT-CODER](https://gitee.com/wapuboy/jygt-coder)自动生成。[JYGT-CODER](https://gitee.com/wapuboy/jygt-coder)是一个自动化代码生成工具，能基于数据库结构自动生成GraphQL风格的WebAPI。运行此工具后，按照向导提供数据库参数，就可以生成对应的Python工程代码。[点击这里了解JYGT-CODER详情](https://gitee.com/wapuboy/jygt-coder)

## 目录结构
生成的代码其顶级目录以数据库名命名，子目录结构如下所示：
```
├─bin   # 执行脚本
├─dat   # 输入输出数据
├─doc   # 相关文档
├─log   # 运行日志
├─src   # 程序代码
└─tmp   # 临时目录
```
其中，子目录src下的目录的结构如下：
```
├─connection    # 数据库连接配置
├─entity        # 数据库表的实体类
├─graphql       # Graphql的对象、查询和修改方法，以及模式定义文件
├─public        # 公共库文件
├─service       # 数据库的服务类
└─test          # 测试用例
```
## 使用说明
1. 测试
进入工程根目录，执行以下命令
```bash
pytest --html=./dat/report.html
```
使用浏览器打开report.html查看测试结果
2. 启动
```bash
python src/main.py
```
3. 访问
浏览器访问：http://localhost:5000/graphql

4. 修改
可参考src的目录结构对象进行修改，然后重新启动并访问
