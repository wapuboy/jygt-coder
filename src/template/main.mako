## 作者： 修炼者 7457222@qq.com 
## 日期： 2024-12-08
## 描述： 用以生成Flask的入口类
<%!
    import public.jygt_coder_utitls as JYGTCoderUtils
    from datetime import datetime   
%><%def name="generate_main(
    db_name,
    host, 
    port,
    prefix
)">${JYGTCoderUtils.get_desc()}
"""
JYGT_CODER 主入口代码

作者：修炼者 (7457222@qq.com)
日期：2024-12-10
说明：
    执行此文件后，将启动Web服务，引导用户完成代码生成。
"""
from flask import Flask, render_template
from flask_graphql import GraphQLView

import sys,os
# 添加当前文件所在目录到Python路径中
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

# 添加包的父目录到Python路径中
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

app = Flask(__name__)

from ${JYGTCoderUtils.get_graphql_schema_file_directory().replace('/', '.').replace('\\', '.')}.${JYGTCoderUtils.get_graphql_schema_object_name(db_name)}  import ${JYGTCoderUtils.get_graphql_schema_object_name(db_name)}

app.add_url_rule('${prefix}',
                 view_func=GraphQLView.as_view(
                    '${JYGTCoderUtils.get_graphql_schema_object_name(db_name)}',
                    schema=${JYGTCoderUtils.get_graphql_schema_object_name(db_name)},
                    graphiql=False
                    ))

@app.route('${prefix}_graphiql')
def graphiql():
    return render_template('custom_graphiql.html')  # No need to specify the 'templates' directory

@app.route('/')
def demo():
    return graphiql()

if __name__ == '__main__':
    app.run(debug=True,port=${port},host='${host}')

</%def>${generate_main(db_name,host, port,prefix)}