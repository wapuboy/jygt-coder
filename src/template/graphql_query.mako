## 作者： 修炼者 7457222@qq.com 
## 日期： 2024-12-08
## 描述： 用以生成数据库表的服务类
<%!
    import public.jygt_coder_utitls as JYGTCoderUtils
    from datetime import datetime
%><%def name="generate_graphql_query(
    table_name, 
    columns,
    indexes
)">${JYGTCoderUtils.get_desc()}
import graphene
from ${JYGTCoderUtils.get_grapql_object_file_directory().replace('/', '.').replace('\\', '.')}.${JYGTCoderUtils.get_graphql_object_class_name(table_name)} import ${JYGTCoderUtils.get_graphql_object_class_name(table_name)},Paginated${JYGTCoderUtils.get_graphql_object_class_name(table_name)}
from ${JYGTCoderUtils.get_service_file_directory().replace('/', '.').replace('\\', '.')}.${JYGTCoderUtils.get_service_class_name(table_name)} import ${JYGTCoderUtils.get_service_class_name(table_name)}

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

class ${JYGTCoderUtils.get_graphql_query_class_name(table_name)}(graphene.ObjectType):

    getAll${JYGTCoderUtils.snake_to_camel_big(table_name)} = graphene.Field(Paginated${JYGTCoderUtils.get_graphql_object_class_name(table_name)},page=graphene.Int(),number=graphene.Int(),description = "获取所有的${JYGTCoderUtils.get_graphql_object_class_name(table_name)}数据，支持分页")

    def resolve_getAll${JYGTCoderUtils.snake_to_camel_big(table_name)}(self, info, page=0, number=10):
        logger.info(f"{self.__class__.__name__}.getAll${JYGTCoderUtils.get_graphql_object_class_name(table_name)}: page={page}, number={number}")
        objService = ${JYGTCoderUtils.get_service_class_name(table_name)}()
        return objService.getAll(page, number)

## 遍历数据库表的索引生成getByIndexName的graphql方法
    % for index_name, index_info in indexes.items():
        <% gql_columns = index_info['gql_columns'] %>
        % if index_info['unique']:
    getBy${JYGTCoderUtils.snake_to_camel_big(index_name)}Of${JYGTCoderUtils.snake_to_camel_big(table_name)}=graphene.Field(
        % else:
    getBy${JYGTCoderUtils.snake_to_camel_big(index_name)}Of${JYGTCoderUtils.snake_to_camel_big(table_name)}=graphene.List(
        % endif
            ${JYGTCoderUtils.get_graphql_object_class_name(table_name)},
            ${'(), '.join(gql_columns)}(),
            description="""
                根据 表${table_name}索引 ${index_name}  获取记录。

                输入参数：
        % for column in index_info['columns']:
                        ${column}
        % endfor
                
                输出参数：
        % if index_info['unique']:
                        ${JYGTCoderUtils.get_entity_class_name(table_name)}对象或 None
        % else:
                        ${JYGTCoderUtils.get_entity_class_name(table_name)}列表或空
        % endif
                """
    )
    def resolve_getBy${JYGTCoderUtils.snake_to_camel_big(index_name)}Of${JYGTCoderUtils.snake_to_camel_big(table_name)}(self, info, ${', '.join(column.split('=')[0] for column in gql_columns)}):
        logger.info(f"{self.__class__.__name__}.getBy${JYGTCoderUtils.snake_to_camel_big(index_name)}Of${JYGTCoderUtils.get_graphql_object_class_name(table_name)}: ${', '.join(index_info['columns'])}")
        objService = ${JYGTCoderUtils.get_service_class_name(table_name)}() 
        return objService.getBy${JYGTCoderUtils.snake_to_camel_big(index_name)}(
            ## ${', '.join(index_info['columns'])}
            ${', '.join(column.split('=')[0] for column in gql_columns)}
        )
    % endfor

</%def>${generate_graphql_query(table_name, columns, indexes)}