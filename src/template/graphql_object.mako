## 作者： 修炼者 7457222@qq.com 
## 日期： 2024-12-08
## 描述： 用以生成数据库表的实体类
<%!
    import public.jygt_coder_utitls as JYGTCoderUtils
    from datetime import datetime   
%><%def name="generate_graphql_object(
    table_name, 
    columns,
    indexes
)">${JYGTCoderUtils.get_desc()}
from graphene_sqlalchemy import SQLAlchemyObjectType
import graphene

from entity.${JYGTCoderUtils.get_entity_class_name(table_name)} import ${JYGTCoderUtils.get_entity_class_name(table_name)}

## graphql object类，基于 sqlalchemy 生成
class ${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(SQLAlchemyObjectType):
    class Meta:
        model = ${JYGTCoderUtils.get_entity_class_name(table_name)}
    
    # 主键本是Int，graphql会将其转化为字符串，这里强制改为int
    % if JYGTCoderUtils.get_primary_from_indexes(indexes):
    ${JYGTCoderUtils.get_primary_from_indexes(indexes)} = graphene.Int()
    % endif
## 分页 grapql object类
class Paginated${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(graphene.ObjectType):
    items = graphene.List(${JYGTCoderUtils.get_graphql_object_class_name(table_name)})
    total = graphene.Int()
    pages = graphene.Int()
    current_page = graphene.Int()

## graphql object input 类
class ${JYGTCoderUtils.get_graphql_input_object_class_name(table_name)}(graphene.InputObjectType):
   
    % for column in columns:
    ${column['name']} = ${column['gql_type']}(${column['gql_required']})
    % endfor

</%def>${generate_graphql_object(table_name, columns,indexes)}