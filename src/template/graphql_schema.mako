## 作者： 修炼者 7457222@qq.com 
## 日期： 2024-12-08
## 描述： 用以生成数据库表的服务类
<%!
    import public.jygt_coder_utitls as JYGTCoderUtils
    from datetime import datetime
%><%def name="generate_graphql_schema(
    db_name,
    table_infos
)">${JYGTCoderUtils.get_desc()}
import graphene

<%  querys = [] %>
<%  mutations = [] %>
%for table_info in table_infos:
    <% table_name = table_info['table_name']   %>
from ${JYGTCoderUtils.get_grapql_mutation_file_directory().replace('/', '.').replace('\\', '.')} import ${JYGTCoderUtils.get_graphql_mutation_class_name(table_name)}
from ${JYGTCoderUtils.get_grapql_query_file_directory().replace('/', '.').replace('\\', '.')}.${JYGTCoderUtils.get_graphql_query_class_name(table_name)} import ${JYGTCoderUtils.get_graphql_query_class_name(table_name)}
    <% querys.append(JYGTCoderUtils.get_graphql_query_class_name(table_name)) %>
    <% mutations.append(f"{JYGTCoderUtils.get_graphql_mutation_class_name(table_name)}.Mutation")%>
% endfor

class AllQuery(${','.join(querys)}
            ,graphene.ObjectType):
  pass


class AllMutation(${','.join(mutations)}
                  ,graphene.ObjectType):
  pass

${JYGTCoderUtils.get_graphql_schema_object_name(db_name)} = graphene.Schema(query=AllQuery,mutation=AllMutation)


</%def>${generate_graphql_schema(db_name, table_infos)}