## 作者： 修炼者 7457222@qq.com 
## 日期： 2024-12-08
## 描述： 用以生成数据库表的服务类
<%!
    import public.jygt_coder_utitls as JYGTCoderUtils
    from datetime import datetime
%><%def name="generate_graphql_mutation(
    table_name, 
    columns,
    indexes
)">${JYGTCoderUtils.get_desc()}
import graphene
from ${JYGTCoderUtils.get_grapql_object_file_directory().replace('/', '.').replace('\\', '.')}.${JYGTCoderUtils.get_graphql_object_class_name(table_name)} import ${JYGTCoderUtils.get_graphql_object_class_name(table_name)},Paginated${JYGTCoderUtils.get_graphql_object_class_name(table_name)},${JYGTCoderUtils.get_graphql_input_object_class_name(table_name)}
from ${JYGTCoderUtils.get_service_file_directory().replace('/', '.').replace('\\', '.')}.${JYGTCoderUtils.get_service_class_name(table_name)} import ${JYGTCoderUtils.get_service_class_name(table_name)}

from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)

## 生成Replace方法，替换一组数据，当数据存在时更新，当数据不存在时增加
class Replace${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(graphene.Mutation):
    class Arguments:
        in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s = graphene.List(${JYGTCoderUtils.get_graphql_input_object_class_name(table_name)},required=True)
        
    res = graphene.List(${JYGTCoderUtils.get_graphql_object_class_name(table_name)})

    def mutate(self, info, in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s):
        
        objService = ${JYGTCoderUtils.get_service_class_name(table_name)}()
        
        return Replace${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(
            res = objService.replace(in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s)
        )
## 生成Add方法，增加一组数据，当数据存在则丢弃
class Add${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(graphene.Mutation):
    class Arguments:
        in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s = graphene.List(${JYGTCoderUtils.get_graphql_input_object_class_name(table_name)},required=True)
        
    res = graphene.List(${JYGTCoderUtils.get_graphql_object_class_name(table_name)})

    def mutate(self, info, in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s):
        
        objService = ${JYGTCoderUtils.get_service_class_name(table_name)}()
        
        return Add${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(
            res = objService.add(in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s)
        )
## 生成Update方法，修改一组数据，当数据不存在则丢弃
class Update${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(graphene.Mutation):
    class Arguments:
        in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s = graphene.List(${JYGTCoderUtils.get_graphql_input_object_class_name(table_name)},required=True)
        
    res = graphene.List(${JYGTCoderUtils.get_graphql_object_class_name(table_name)})

    def mutate(self, info, in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s):
        
        objService = ${JYGTCoderUtils.get_service_class_name(table_name)}()
        
        return Update${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(
            res = objService.update(in${JYGTCoderUtils.get_graphql_object_class_name(table_name)}s)
        )
## 生成支持按照主键删除的方法，支持单值和列表删除
% if JYGTCoderUtils.get_primary_from_indexes(indexes):
class Delete${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(graphene.Mutation):
    class Arguments:
    % for index_name, index_info in indexes.items():
        % if index_name == 'PRIMARY':
        ${f"{index_info['columns'][0]}"}s = graphene.List(${index_info['gql_columns'][0].split('=', 1)[1]},required=True)
       
    res = graphene.List(${JYGTCoderUtils.get_graphql_object_class_name(table_name)})

    def mutate(self, info, ${f"{index_info['columns'][0]}"}s):
        objService = ${JYGTCoderUtils.get_service_class_name(table_name)}()
        return Delete${JYGTCoderUtils.get_graphql_object_class_name(table_name)}(
            res = objService.delete(${f"{index_info['columns'][0]}"}s)
        ) 
        % endif
    % endfor
% endif

class Mutation(graphene.ObjectType):
    delete${JYGTCoderUtils.snake_to_camel_big(table_name)} = Delete${JYGTCoderUtils.get_graphql_object_class_name(table_name)}.Field()

    add${JYGTCoderUtils.snake_to_camel_big(table_name)} = Add${JYGTCoderUtils.get_graphql_object_class_name(table_name)}.Field()

    update${JYGTCoderUtils.snake_to_camel_big(table_name)} = Update${JYGTCoderUtils.get_graphql_object_class_name(table_name)}.Field()

    replace${JYGTCoderUtils.snake_to_camel_big(table_name)} = Replace${JYGTCoderUtils.get_graphql_object_class_name(table_name)}.Field()

</%def>${generate_graphql_mutation(table_name, columns, indexes)}