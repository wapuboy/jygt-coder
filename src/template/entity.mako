## 作者： 修炼者 7457222@qq.com 
## 日期： 2024-12-08
## 描述： 用以生成数据库表的实体类
<%!
    import public.jygt_coder_utitls as JYGTCoderUtils
    from datetime import datetime   
%><%def name="generate_model(
    table_name, 
    columns
)">${JYGTCoderUtils.get_desc()}
from sqlalchemy import Column, Integer, String, DateTime,Float,Text,BigInteger,Numeric,Date,Time,Boolean,LargeBinary,func
from sqlalchemy.orm import declarative_base
from datetime import datetime

Base = declarative_base()

class ${JYGTCoderUtils.get_entity_class_name(table_name)}(Base):
    __tablename__ = '${table_name}'
    % for column in columns:
    ${column['name']} = Column(${column['type']}, ${column['primary_key']} ${column['nullable']}${'' if (column['nullable']=='nullable=True' or column['default']=='default=None') else f",  {column['default']}"})
    % endfor

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def __repr__(self):
        attrs = ', '.join(f"{key}={value}" for key, value in vars(self).items() if not key.startswith('_'))
        return f"${JYGTCoderUtils.get_entity_class_name(table_name)}({attrs})"
</%def>${generate_model(table_name, columns)}