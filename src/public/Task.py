"""
task 定义了数据库环境、WebAPI的配置参数、目标代码版本、存放目的地等信息。用于生成代码

作者：修炼者 7457222@qq.com
日期：2024-12-10 10:48:19
"""
from public.DBInfo import DBInfo
from public.WebAPIInfo import WebAPIInfo
from public.TargetProjectInfo import TargetProjectInfo
from public.jygt_coder_loger import get_logger

logger = get_logger(__name__)

class Task:
    """
    管理代码生成任务
    """
    def __init__(self,db_info:DBInfo,api_info:WebAPIInfo,target_project_info:TargetProjectInfo):
        self.db_info = db_info
        self.api_info = api_info
        self.target_project_info = target_project_info
    
    def getDBInfo(self):
        return self.db_info
    
    def getWebAPIInfo(self):
        return self.api_info
    
    def getTargetProjectInfo(self):
        return self.target_project_info
    
    def __repr__(self) -> str:
        return f"<Task(\n\tdb_info={self.db_info}, \n\tapi_info={self.api_info}, \n\ttarget_project_info={self.target_project_info}\n)>"
    
    def init(self)->bool:
        try:
            self.getDBInfo().loadInfo()
            self.getTargetProjectInfo().initTargetProject()
            
            return True
        except Exception as e:
            logger.error(f"init error:{e}")
            return False
        finally:
            pass