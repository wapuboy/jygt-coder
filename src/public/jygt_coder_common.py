import random
import time,datetime
import os 
import threading
import json

# 协助json dumps处理datetime类型
class MyEncoder(json.JSONEncoder):
    def default(self, o) :
      if isinstance(o,datetime.datetime):
        return o.isoformat()
      return json.JSONEncoder.default(self,o)
    

# 生成一个指定长度的随机字符串
def getRandomString(length):
  lettter = "0123456789ABCDEFJHIJKLMNOPQRSTUVWXYZ0"
  res = ""
  while len(res) < length:
    res += lettter[random.randint(0,36)]
  return res 

def getYYYYMMDDhhmmss():
  # 获取当前时间
  current_time = datetime.datetime.now()

  # 格式化当前时间为指定的字符串格式
  return current_time.strftime('%Y-%b-%d %H:%M:%S')

# 生成一个指定长度的随机字符串
def getRandomNumber(length):
  lettter = "0234568902345689023456890234568902345689023456890234568902345689023456890234568902345689"
  res = ""
  while len(res) < length:
    res += lettter[random.randint(0,36)]
  return res 

# 生成一个按时间的流水号，尽量不重复
vno=0
def getVno():
  timestamp = str(int(time.time()))
  global vno
  vno+=1
  if vno > 999999 :
    vno = 0
  
  return "{}-{}-{}-{}".format(timestamp,os.getpid(),threading.get_ident(),vno);

def getPTID(prefix =  None):
  if prefix is None:
    prefix = "PTID"
  return "{}-{}-{}".format(prefix,os.getpid(),threading.get_ident())
