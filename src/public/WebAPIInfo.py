"""
WebAPIInfo 定义了WebAPI的参数，当前只考虑Flask

作者：修炼者 7457222@qq.com
日期：2024-12-10 10:48:19
"""
class WebAPIInfo:
    """
    WebAPIInfo 定义了WebAPI的参数，当前只考虑Flask
    """
    def __init__(self, host, port, path, title="JYGT-CODER", heading="欢迎使用 JYGT-CODER ！", message="基于pymysql,sqlalchemy,flask,graphq构建"):
        self.host = host
        self.port = port
        self.path = path

        # 如下几个是首页的个性化信息
        self.title = title
        self.heading = heading
        self.message = message
    

    def getHost(self):
        return self.host
    
    def getPath(self):
        return self.path
    
    def getTitle(self):
        return self.title

    def getHeading(self):
        return self.heading
    
    def getMessage(self):
        return self.message
    
    def getPort(self):
        return self.port
    
    def __repr__(self) -> str:
        return f"<WebAPIInfo(host={self.host}, port={self.port}, path={self.path}>"
    
        #return f"WebAPIInfo(host={self.host}, port={self.port}, path={self.path}, title={self.title}, heading={self.heading}, message={self.message})"
    
        