"""
TargetProjectInfo 定义了目标工程的版本，存放位置等信息。

作者：修炼者 7457222@qq.com
日期：2024-12-10 10:48:19
"""
import os
import shutil



class TargetProjectInfo:
    """
    TargetProjectInfo 定义了目标工程的版本，存放位置等信息
    """
    def __init__(self, root_dir:str,db_name:str,ver="1.0.0"):
        self.dir = os.path.join(root_dir,db_name)
        self.ver = ver
    def getRootDir(self):
        return self.dir
    def getVer(self):
       return self.ver
     
    def __getInitSourceDir(self):
        current_dir = os.path.dirname(os.path.abspath(__file__))
        parent_dir = os.path.dirname(current_dir)
        return os.path.join(parent_dir,'template','assets')
    
    def getDirOfBin(self,subdir=''):
        subdirs = subdir.split('/')
        return os.path.join(self.dir,'bin',*subdirs)
    def getDirOfDat(self,subdir=''):
        subdirs = subdir.split('/')
        return os.path.join(self.dir,'dat',*subdirs)
    def getDirOfDoc(self,subdir=''):
        subdirs = subdir.split('/')
        return os.path.join(self.dir,'doc',*subdirs)    
    def getDirOfLog(self,subdir=''):
        subdirs = subdir.split('/')
        return os.path.join(self.dir,'log',*subdirs)
    def getDirOfSrc(self,subdir=''):
        subdirs = subdir.split('/')
        resDir = os.path.join(self.dir,'src',*subdirs)  
        return resDir
    def getDirOfTmp(self,subdir=''):
        subdirs = subdir.split('/')
        return os.path.join(self.dir,'tmp',*subdirs)  
    
    def __repr__(self)-> str:
        return f"<TargetProjectInfo(dir={self.dir},ver={self.ver})>"
    
    def save(self,content,filename,directory):
        if not os.path.exists(directory):
            os.makedirs(directory)

        with open(os.path.join(directory, filename), "w",encoding='utf-8') as f:
            f.write(content)

    def __copy(self,src_dir, dest_dir):
        # 检查源目录是否存在
        if not os.path.exists(src_dir):
            # print(f"源目录 '{src_dir}' 不存在。")
            return False

        # 如果目标目录不存在，则创建它
        os.makedirs(dest_dir, exist_ok=True)

        # 如果源文件是一个文件
        if not os.path.isdir(src_dir):
            shutil.copy2(src_dir, dest_dir)
            return True

        # 将源目录的内容拷贝到目标目录
        for item in os.listdir(src_dir):
            if item.find("__pycache__") >= 0:
                continue
            
            src_path = os.path.join(src_dir, item)
            dest_path = os.path.join(dest_dir, item)

            # 如果是目录，使用 copytree；如果是文件，使用 copy2
            if os.path.isdir(src_path):
                shutil.copytree(src_path, dest_path)
            else:
                shutil.copy2(src_path, dest_path)
    def __rmdir(self, directory:str):
        if directory.find('dist') < 0 :
            raise Exception("禁止删除非dist目录")
            return
        if os.path.exists(directory):
            shutil.rmtree(directory)  # 删除目录及其所有内容
            # print(f"已删除: {directory} 及其所有文件和子目录。")
        else:
            # print(f"目录 '{directory}' 不存在。")
            pass
    def initTargetProject(self):
        # 清空目标工程
        self.__rmdir(self.dir)
        # 初始化目标工程
        init_source_dir = self.__getInitSourceDir()
        self.__copy(init_source_dir,self.dir)
    
    def dumpRootDir(self):
        print(f"{'-'*60}")
        for root, dirs, files in os.walk(self.dir):
            level = root.replace(self.dir, '').count(os.sep)
            indent = ' ' * 4 * (level)  # Indentation for the tree structure
            print(f"{indent}{os.path.basename(root)}/")  # Print the directory name
            subindent = ' ' * 4 * (level + 1)
            for f in files:
                print(f"{subindent}{f}")  # Print the file name
        
        print(f"{'-'*60}")