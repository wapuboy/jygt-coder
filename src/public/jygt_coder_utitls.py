# jygt_coder_utitls.py
"""
jygt_coder_utitls.py

作者: 修炼者 7457222@qq.com
日期: 2024-12-08
描述: 定义CODER的常规约定，包括对类、文件名称的处理。
用法：
    import utils as JYGTCoderUtils

    JYGTCOderUtils.get_entity_class_name('t_user','db_jygt')
    JYGTCOderUtils.get_service_class_name('t_user','db_jygt')
"""
import re
import urllib.parse
from datetime import datetime

import os

def get_coder_root_dir():
    """
    描述: 获取JYGT-CODER的根目录
    """
    # 添加当前文件所在目录
    current_dir = os.path.dirname(os.path.abspath(__file__))
    parent_dir = os.path.dirname(current_dir)
    root_dir = os.path.dirname(parent_dir)

    return root_dir

def get_coder_template_dir():
    """
    描述: 获取JYGT-CODER的模板目录
    """
    return os.path.join(get_coder_root_dir(),'src', 'template')

def snake_to_camel(snake_str):
    """
    描述: 将字符串从snake形式转化为普通驼峰形式
    输入: snake_str , 形如snake_case_string
    输出: camel_str , 形如camelCaseString
    """
    components = snake_str.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])

def snake_to_camel_big(snake_str):
    """
    描述: 将字符串从snake形式转化为首字母大写的驼峰形式
    输入: snake_str , 形如snake_case_string
    输出: camel_str , 形如camelCaseString
    """
    components = snake_str.split('_')
    return ''.join(x.title() for x in components)

def camel_to_snake(camel_str):
    """
    描述: 将字符串从camel形式转化为snake形式
    输入: camel_str , 形如CamelCaseString or camelCaseString
    输出: snake_str , 形如camel_case_string
    """
    snake_str = re.sub(r'([A-Z])', r'_\1', camel_str)
    return snake_str.lower().lstrip('_')

def get_entity_class_name(table_name, db_name=""):
    """
    描述: 基于table_name,db_name获取实体类的类名
    输入: 
        table_name , 形如t_user
        db_name , 形如db_jygt
    输出: 实体类名 , 形如TUserEntity
    """
    prefix = f"{db_name}_" if db_name else ""
    return snake_to_camel_big(prefix + table_name+ "_entity")

def get_entity_file_name(table_name, db_name=""):
    """
    描述: 基于table_name,db_name获取实体类的文件名
    输入: 
        table_name , 形如t_user
        db_name , 形如db_jygt
    输出: 实体类的文件名 , 形如TUserEntity.py
    """
    return f"{get_entity_class_name(table_name,db_name)}.py"

def get_service_class_name(table_name, db_name=""):
    """
    描述: 基于table_name,db_name获取服务类的类名
    输入: 
        table_name , 形如t_user
        db_name , 形如db_jygt
    输出: 实体类名 , 形如TUserService
    """
    prefix = f"{db_name}_" if db_name else ""
    return snake_to_camel_big(prefix + table_name + "_service")

def get_service_file_name(table_name, db_name=""):
    """
    描述: 基于table_name,db_name获取服务类的文件名
    输入: 
        table_name , 形如t_user
        db_name , 形如db_jygt
    输出: 实体的文件名 , 形如TUserService.py
    """
    return f"{get_service_class_name(table_name,db_name)}.py"
def get_test_file_name(table_name, db_name=""):
    """
    描述: 基于table_name,db_name获取测试服务类的文件名
    输入: 
        table_name , 形如t_user
        db_name , 形如db_jygt
    输出: 实体的文件名 , 形如test_TUserService.py
    """
    return f"test_{get_service_class_name(table_name,db_name)}.py"


def get_connection_class_name(db_name=""):
    """
    描述: 基于db_name获取连接的文件名
    输入: 
        table_name , 形如t_user
        db_name , 形如db_jygt
    输出: 实体的文件名 , 形如TUserService.py
    """
    prefix = (f"${db_name}_" if db_name else "")
    return snake_to_camel_big(f"{prefix}mydb_pool")
def get_connection_file_name(db_name=""):
    """
    描述: 基于db_name获取连接的文件名
    输入: 
        table_name , 形如t_user
        db_name , 形如db_jygt
    输出: 实体的文件名 , 形如TUserService.py
    """
    return f"{get_connection_class_name()}.py"
def get_connection_file_directory():
    return f"connection"
def get_service_file_directory():
    return f"service"
def get_test_file_directory():
    return f"test"
def get_entity_file_directory():
    return f"entity"
def get_current_time():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def get_primary_from_indexes(indexes):
    if 'PRIMARY' in indexes:
        return indexes['PRIMARY']['columns'][0]
    else:
        return None
def get_desc():
    # 定义模板字符串
    template = '''\
#
# 此代码为自动化代码工具【JYGT-CODER】自动生成的文件，请勿手动编辑。              
#                                                               
# 作者: 修炼者 7457222@qq.com                                    
# 日期: {generated_date}                                        
#'''

    # 获取当前日期时间并格式化为字符串
    generated_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # 使用当前日期时间填充模板
    generated_code = template.format(generated_date=generated_date)

    return generated_code


def get_graphql_object_class_name(table_name, db_name=""):
    prefix = f"{db_name}_" if db_name else ""
    return snake_to_camel_big(prefix + table_name+ "_GQL_Object")

def get_graphql_input_object_class_name(table_name, db_name=""):
    prefix = f"{db_name}_" if db_name else ""
    return snake_to_camel_big(prefix + table_name+ "_GQL_input_Object")

def get_graphql_object_file_name(table_name, db_name=""):
    return f"{get_graphql_object_class_name(table_name,db_name)}.py"

def get_grapql_object_file_directory():
    return f"mygraphql/object"

def get_graphql_query_class_name(table_name, db_name=""):
    prefix = f"{db_name}_" if db_name else ""
    return snake_to_camel_big(prefix + table_name+ "_GQL_QUery")
def get_graphql_query_file_name(table_name, db_name=""):
    return f"{get_graphql_query_class_name(table_name,db_name)}.py"

def get_grapql_query_file_directory():
    return f"mygraphql/query"

def get_graphql_mutation_class_name(table_name, db_name=""):
    prefix = f"{db_name}_" if db_name else ""
    return snake_to_camel_big(prefix + table_name+ "_GQL_Mutation")
def get_graphql_mutation_file_name(table_name, db_name=""):
    return f"{get_graphql_mutation_class_name(table_name,db_name)}.py"

def get_grapql_mutation_file_directory():
    return f"mygraphql/mutation"

def get_graphql_schema_object_name(db_name=""):
    return snake_to_camel(f"{db_name}_schema")
def get_graphql_schema_file_name(db_name=""):
    return f"{get_graphql_schema_object_name(db_name)}.py"

def get_graphql_schema_file_directory():
    return f"mygraphql/schema"
def get_main_file_name():
    return f"main.py"
def get_graphql_customer_iql_file_directory():
    return f"templates"
def get_graphql_customer_iql_file_name():
    return f"custom_graphiql.html"