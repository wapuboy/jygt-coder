"""
文件管理模块

作者：修炼者 (7457222@qq.com)
日期：2024-12-10

用法：
    import public.jygt_coder_file as JygtCoderFile
    JygtCoderFile.copy('src/public', 'dist/public')
"""
import shutil
import os

def copy(src_dir, dest_dir):
    # 检查源目录是否存在
    if not os.path.exists(src_dir):
        # print(f"源目录 '{src_dir}' 不存在。")
        return False

    # 如果目标目录不存在，则创建它
    os.makedirs(dest_dir, exist_ok=True)

    # 如果源文件是一个文件
    if not os.path.isdir(src_dir):
        shutil.copy2(src_dir, dest_dir)
        return True

    # 将源目录的内容拷贝到目标目录
    for item in os.listdir(src_dir):
        if item.find("__pycache__") >= 0:
            continue
        
        src_path = os.path.join(src_dir, item)
        dest_path = os.path.join(dest_dir, item)

        # 如果是目录，使用 copytree；如果是文件，使用 copy2
        if os.path.isdir(src_path):
            shutil.copytree(src_path, dest_path)
        else:
            shutil.copy2(src_path, dest_path)

    #print(f"所有文件和目录已从 '{src_dir}' 拷贝到 '{dest_dir}'。")
    return True