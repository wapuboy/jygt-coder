"""
获取任务描述。
读取数据库表、字段和索引信息
初始化目标工程目录

作者：修炼者 7457222@qq.com
日期: 2024-12-10 10:48:19
"""
from public.Task import Task
from public.DBInfo import DBInfo
from public.WebAPIInfo import WebAPIInfo
from public.TargetProjectInfo import TargetProjectInfo

from public.jygt_coder_loger import get_logger

logger = get_logger(__name__)
"""
获取任务描述

作者：修炼者 7457222@qq.com
日期：2024-12-10 10:48:19
"""
def getTaskDesc(
        db_host="localhost",
        db_port=3306,
        db_user="root",
        db_password="root1234",
        db_database="db_jygt",

        api_host="localhost",
        api_port=5000,
        api_path="/api",

        tar_root_dir="dist",
        tar_ver="1.0.0"
)->Task:
    try:

        dbInfo = DBInfo(db_host,db_port,db_user,db_password,db_database)
        
        # dbInfo.loadInfo()

        apiInfo = WebAPIInfo(api_host,api_port,api_path)

        targetProjectInfo = TargetProjectInfo(tar_root_dir,db_database,tar_ver)
        # targetProjectInfo.initTargetProject()

        return Task(dbInfo,apiInfo,targetProjectInfo)
    except Exception as e:
        logger.error(f"getTaskDesc error:{e}")
        return None
    finally:
        pass
        
