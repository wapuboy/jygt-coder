from public.jygt_coder_loger import get_logger 

logger = get_logger(__name__)


from flask import Blueprint,request

bp = Blueprint('index',__name__)


from mako.template import Template
from mako.exceptions import TemplateLookupException, SyntaxException
from mako.lookup import TemplateLookup
import os


@bp.route("/",methods=['GET','POST'])
def index():
    logger.info("request args is {}".format(request.args))  

    # 使用 Mako 渲染模板
    current_dir = os.path.dirname(__file__)  # 获取当前文件的目录
    lookup = TemplateLookup(
      directories=[os.path.join(current_dir, 'template'), 
               os.path.join(current_dir, '../../dat/', 'template')
               ])
    template = lookup.get_template('index.html.mako')

    # 定义要传递给模板的变量
    title = "JYGT-CODER"
    heading = "欢迎使用 JYGT-CODER ！"
    message = "使用pymysql,sqlalchemy,flask,graphql构建！"

    return template.render(title=title, heading=heading, message=message)
  

