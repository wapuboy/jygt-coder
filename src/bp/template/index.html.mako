<!DOCTYPE html>
<html lang="cn" charset="utf-8">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title}</title>
</head>
<body>
    <h1>${heading}</h1>
    <p>${message}</p>
</body>
</html>