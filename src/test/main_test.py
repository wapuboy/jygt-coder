import os
import sys

# 添加当前文件所在目录到Python路径中
current_dir = os.path.dirname(os.path.abspath(__file__))
#sys.path.append(current_dir)

# 添加包的父目录到Python路径中
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)


def copy_test():
    import public.jygt_coder_file as JygtCoderFile

    srcDir = os.path.join(parent_dir, 'public', 'jygt_coder_common.py')
    dstDir = os.path.join(parent_dir, '..','dat','dist','db_jygt', 'public')

    print(JygtCoderFile.copy(srcDir,dstDir))


    srcDir = os.path.join(parent_dir, 'public')
    print(JygtCoderFile.copy(srcDir,dstDir))

def task_test():
    from process.get_task_desc import getTaskDesc
    taskInfo = getTaskDesc()
    print(taskInfo)

task_test()