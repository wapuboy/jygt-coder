import argparse

def main():
    parser = argparse.ArgumentParser(description="A simple command-line tool.")
    parser.add_argument('-f', '--file', type=str, help='The file to process')
    parser.add_argument('-v', '--verbose', action='store_true', help='Increase output verbosity')

    args = parser.parse_args()

    if args.verbose:
        print("Verbose mode is on.")

    if args.file:
        print(f"Processing file: {args.file}")

if __name__ == "__main__":
    main()