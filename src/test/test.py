import os
import sys

# 添加当前文件所在目录到Python路径中
current_dir = os.path.dirname(os.path.abspath(__file__))
#sys.path.append(current_dir)

# 添加包的父目录到Python路径中
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import public.jygt_coder_utitls as jygt

print(jygt.snake_to_camel_big("hello_world"))
print(jygt.snake_to_camel_big("t_table"))
print(jygt.snake_to_camel_big("table"))

print(jygt.snake_to_camel_big("PRIMARY"))

print(jygt.snake_to_camel_big("Gauss_GGGG"))

print(jygt.snake_to_camel_big("GUUU_GaUS"))