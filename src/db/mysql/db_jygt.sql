/*
演示用MySQL数据库脚本

## 作者： 修炼者 7457222@qq.com 
## 日期： 2024-12-08
## 描述： 用以生成数据库表的实体类
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_button
-- ----------------------------
DROP TABLE IF EXISTS `t_button`;
CREATE TABLE `t_button`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单的名称',
  `f_group_id` int(11) NULL DEFAULT NULL COMMENT '分组id',
  `f_label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的名称',
  `f_icon` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的图标',
  `f_action` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后的动作',
  `f_paras` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后action携带的参数',
  `f_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源的描述',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '状态（可用1/不可用0）',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_group_id`(`f_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_element
-- ----------------------------
DROP TABLE IF EXISTS `t_element`;
CREATE TABLE `t_element`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '元素名 ： 唯一标记一个元素，系统基于此名称实现系统支持的基本数据要素，定义其属性、方法、事件，以及存储方式',
  `f_label` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示给用户的元素名称，没有时直接显示name',
  `f_group_id` int(11) NOT NULL DEFAULT 0,
  `f_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '元素的基本描述',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '状态 1 有效 0 无效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_group_id`(`f_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_field
-- ----------------------------
DROP TABLE IF EXISTS `t_field`;
CREATE TABLE `t_field`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '字段名，唯一标志一个字段',
  `f_label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示给用户看的名称，空的时候直接使用name',
  `f_group_id` int(11) NOT NULL COMMENT '分组id，对应t_group的主键',
  `f_icon` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '字段的icon',
  `f_type` int(11) NOT NULL COMMENT '字段类型：1 简单类型只包含一个子元素， 2 组合类型包含一个或多个子元素',
  `f_element_id` int(11) NULL DEFAULT NULL COMMENT 'type为1时，只包含一个元素时，这里是元素的主键；其他无意义',
  `f_format` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字段的格式定义，一般使用正则表达式',
  `f_value` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字段的默认值',
  `f_state` int(11) NULL DEFAULT NULL COMMENT '状态 1 有效 0 无效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建时间',
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '记录修改时间',
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_group_id_and_name`(`f_group_id`, `f_name`) USING BTREE,
  INDEX `idx_element_id`(`f_element_id`) USING BTREE,
  INDEX `idx_group_id`(`f_group_id`) USING BTREE,
  INDEX `unq_group_id_and_label`(`f_group_id`, `f_label`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_field_content
-- ----------------------------
DROP TABLE IF EXISTS `t_field_content`;
CREATE TABLE `t_field_content`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_field_id` int(11) NOT NULL COMMENT '字段名，唯一标志一个字段',
  `f_content_id` int(11) NOT NULL COMMENT '当content_type为1时，是element的主键,2 是field的主键',
  `f_content_type` int(11) NULL DEFAULT 1 COMMENT '1 element , 2 field',
  `f_index` int(11) NULL DEFAULT NULL COMMENT 'content_id的序号',
  `f_mandatory` int(11) NOT NULL DEFAULT 0 COMMENT '0 可选 1 必填',
  `f_value` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字段的默认值',
  `f_state` int(11) NULL DEFAULT 0 COMMENT '状态 1 有效 0 无效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建时间',
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '记录修改时间',
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_field_id_and_content_id`(`f_content_id`, `f_field_id`) USING BTREE,
  INDEX `idx_content_id`(`f_content_id`) USING BTREE,
  INDEX `idx_field_id`(`f_field_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_group
-- ----------------------------
DROP TABLE IF EXISTS `t_group`;
CREATE TABLE `t_group`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '分组名',
  `f_label` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分组显示的名称，为空时使用name',
  `f_type` int(11) NOT NULL DEFAULT 0 COMMENT '分组使用的类型： 1 字段、2 按钮、3 接口、4 页面、5 菜单、6 角色等，0为万能分组',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_name`(`f_name`) USING BTREE,
  UNIQUE INDEX `unq_type_and_name`(`f_name`, `f_type`) USING BTREE,
  INDEX `unq_type_and_label`(`f_label`, `f_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_interface
-- ----------------------------
DROP TABLE IF EXISTS `t_interface`;
CREATE TABLE `t_interface`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单的名称',
  `f_label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的名称',
  `f_icon` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的图标',
  `f_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后的目标url',
  `f_paras` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后url携带的参数',
  `f_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源的描述',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '状态（可用1/不可用0）',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_name`(`f_name`) USING BTREE,
  INDEX `idx_label`(`f_label`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_kyx_template
-- ----------------------------
DROP TABLE IF EXISTS `t_kyx_template`;
CREATE TABLE `t_kyx_template`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_entity_id` int(11) NULL DEFAULT NULL COMMENT '实体的主键',
  `f_entity_type` int(11) NOT NULL DEFAULT 0 COMMENT '实体的类型：角色、关系',
  `f_field_id` int(11) NULL DEFAULT NULL COMMENT '字段id',
  `f_index` int(11) NULL DEFAULT NULL COMMENT '序号：字段排列的顺序编码 从1 开始；  0 表示无需排序',
  `f_mandatory` int(11) NULL DEFAULT NULL COMMENT '1 必填  0 可选',
  `f_default_value` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字段的默认值',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_type_and_entity_id_and_field_id`(`f_entity_type`, `f_entity_id`, `f_field_id`) USING BTREE,
  INDEX `idx_type_and_entity_id`(`f_entity_id`, `f_entity_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单的名称',
  `f_parent_id` int(11) NULL DEFAULT NULL COMMENT '菜单的父级菜单，0表示自己为顶级',
  `f_label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的名称',
  `f_icon` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的图标',
  `f_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后的目标url',
  `f_paras` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后url携带的参数',
  `f_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源的描述',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '状态（可用1/不可用0）',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_name`(`f_name`) USING BTREE,
  INDEX `idx_parent_id`(`f_parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 97 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_page
-- ----------------------------
DROP TABLE IF EXISTS `t_page`;
CREATE TABLE `t_page`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单的名称',
  `f_label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的名称',
  `f_icon` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示的图标',
  `f_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后的目标url',
  `f_paras` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '点击后url携带的参数',
  `f_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源的描述',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '状态（可用1/不可用0）',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_name`(`f_name`) USING BTREE,
  INDEX `idx_label`(`f_label`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_relation
-- ----------------------------
DROP TABLE IF EXISTS `t_relation`;
CREATE TABLE `t_relation`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '关系名',
  `f_type` int(11) NOT NULL COMMENT '0 单向设置  1 双向确认。  \r\n	关注，屏蔽就是单向的；  同一用户是双向的；  雇员是双向的；供应商、客户是单向的',
  `f_desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户的头像url',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_user_id` int(11) NOT NULL DEFAULT 0 COMMENT '0 系统设定；  其他为用户自定义',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `unq_name`(`f_name`) USING BTREE,
  INDEX `idx_type`(`f_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_relation_relation
-- ----------------------------
DROP TABLE IF EXISTS `t_relation_relation`;
CREATE TABLE `t_relation_relation`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_relation_id_1` int(11) NOT NULL COMMENT '关系种类1',
  `f_type` int(11) NOT NULL COMMENT '关系1 与关系2： 0 互斥， 1 共存 ',
  `f_relation_id_2` int(11) NOT NULL DEFAULT 0 COMMENT '关系种类2 ： 0 代表所有关系 其他代表具体的关系',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_r1_and_type_and_r2`(`f_relation_id_1`, `f_type`, `f_relation_id_2`) USING BTREE,
  INDEX `idx_r2`(`f_relation_id_2`) USING BTREE,
  INDEX `idx_r1`(`f_relation_id_1`) USING BTREE,
  INDEX `idx_r1_and_type`(`f_relation_id_1`, `f_type`) USING BTREE,
  INDEX `idx_type_and_r2`(`f_type`, `f_relation_id_2`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_resource`;
CREATE TABLE `t_resource`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_resource_id` int(11) NOT NULL COMMENT '资源ID，业务语义上，与f_resource_type一起唯一标记一种资源',
  `f_resource_type` int(11) NOT NULL COMMENT '资源类型： 1 菜单 ，2 页面，3 按钮，4 接口，5 字段，6 应用等',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '资源的名字',
  `f_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源的描述',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '状态（可用1/不可用0）',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_type_and_resource_id`(`f_resource_type`, `f_resource_id`) USING BTREE,
  INDEX `idx_resource_id`(`f_resource_id`) USING BTREE,
  INDEX `idx_type`(`f_resource_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '名称',
  `f_group_id` int(11) NOT NULL DEFAULT 0,
  `f_desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户座右铭',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_name`(`f_name`) USING BTREE,
  INDEX `idx_group_id`(`f_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_role_resource`;
CREATE TABLE `t_role_resource`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_role_id` int(11) NULL DEFAULT NULL COMMENT '名称',
  `f_resource_id` int(11) NOT NULL DEFAULT 0,
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_role_id_and_resource_id`(`f_role_id`, `f_resource_id`) USING BTREE,
  INDEX `idx_resource_id`(`f_resource_id`) USING BTREE,
  INDEX `idx_role_id`(`f_role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_uid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户ID，业务语义上，与f_user_source_id一起唯一标记一个用户',
  `f_user_source_id` int(11) NOT NULL COMMENT '用户来源id，对应t_user_source表的相关记录主键',
  `f_auth_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户身份识别码，一般用来验证用户的身份。例如密码，手机验证码、邮件验证码、或者来自第三方平台的授权吗',
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户名',
  `f_motto` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户座右铭',
  `f_avarta_url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户的头像url',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_source_and_uid`(`f_uid`, `f_user_source_id`) USING BTREE,
  INDEX `idx_source`(`f_user_source_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user_kyd
-- ----------------------------
DROP TABLE IF EXISTS `t_user_kyd`;
CREATE TABLE `t_user_kyd`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_user_id` int(11) NULL DEFAULT NULL COMMENT '实体的主键',
  `f_field_id` int(11) NULL DEFAULT NULL COMMENT '字段id',
  `f_index` int(11) NULL DEFAULT NULL COMMENT '序号：字段排列的顺序编码 从1 开始；  0 表示无需排序',
  `f_value` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字段的默认值',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_uid`(`f_user_id`) USING BTREE,
  INDEX `idx_fid`(`f_field_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 98 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user_log
-- ----------------------------
DROP TABLE IF EXISTS `t_user_log`;
CREATE TABLE `t_user_log`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_user_id` int(11) NOT NULL COMMENT '用户ID，业务语义上，与f_user_source_id一起唯一标记一个用户',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_type` int(11) NULL DEFAULT NULL COMMENT '1 C 2 R 3 U 4 D',
  `f_message` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户座右铭',
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_uid`(`f_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `t_user_relation`;
CREATE TABLE `t_user_relation`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_user_id_1` int(11) NOT NULL COMMENT '主动方用户id',
  `f_relation_id` int(11) NOT NULL COMMENT '关系id',
  `f_user_id_2` int(11) NOT NULL COMMENT '被动方用户id',
  `f_desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述信息',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_uid1_and_rid_and_uid2`(`f_user_id_1`, `f_relation_id`, `f_user_id_2`) USING BTREE,
  INDEX `idx_uid1`(`f_user_id_1`) USING BTREE,
  INDEX `idx_uid1_and rid`(`f_user_id_1`, `f_relation_id`) USING BTREE,
  INDEX `idx_uid2`(`f_user_id_2`) USING BTREE,
  INDEX `idx_uid2_and_rid`(`f_relation_id`, `f_user_id_2`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键：唯一标识特定表的一个记录',
  `f_user_id` int(11) NOT NULL COMMENT '名称',
  `f_role_id` int(11) NOT NULL DEFAULT 0,
  `f_desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注信息',
  `f_state` int(11) NOT NULL DEFAULT 0 COMMENT '0 无效 1 有效',
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  UNIQUE INDEX `unq_uid_and_role_id`(`f_user_id`, `f_role_id`) USING BTREE,
  INDEX `idx_uid`(`f_user_id`) USING BTREE,
  INDEX `idx_role_id`(`f_role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user_source
-- ----------------------------
DROP TABLE IF EXISTS `t_user_source`;
CREATE TABLE `t_user_source`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `f_paras` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `f_desc` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `f_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_modify_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`f_id`) USING BTREE,
  INDEX `idx_name`(`f_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户来源' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
