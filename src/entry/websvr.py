"""
JYGT_CODER 主入口代码

作者：修炼者 (7457222@qq.com)
日期：2024-12-10
说明：
    执行此文件后，将启动Web服务，引导用户完成代码生成。
"""
from flask import Flask
from flask_graphql import GraphQLView

import sys,os
# 添加当前文件所在目录到Python路径中
current_dir = os.path.dirname(os.path.abspath(__file__))
#sys.path.append(current_dir)

# 添加包的父目录到Python路径中
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

app = Flask(__name__)


# /index /

from bp import index_html 
app.register_blueprint(index_html.bp,url_prefix='')


# graphql 命名规则 ：  schema_$entity ;   gql_$entity
# https://fkx.aigrow.space/graphql
"""
from interface.graphql.schema.AppSchema import schema_app

app.add_url_rule('/graphql',
                 view_func=GraphQLView.as_view(
                    'gql_app',
                    schema=schema_app,
                    graphiql=True
                    ))

"""
if __name__ == '__main__':
    app.run(debug=True,port=5000,host='127.0.0.1')