"""
获取任务描述,并读取数据库表、字段和索引信息

作者：修炼者 7457222@qq.com
日期: 2024-12-10 10:48:19
"""
import os
import sys

# 添加当前文件所在目录到Python路径中
current_dir = os.path.dirname(os.path.abspath(__file__))
#sys.path.append(current_dir)

# 添加包的父目录到Python路径中
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import argparse
from process.get_task_desc import getTaskDesc
from process.generate_code import generateCode

def main():
    parser = argparse.ArgumentParser(description="JYGT-CODER 命令行工具，用于获取任务描述，并读取数据库表、字段和索引信息.")
    
    parser.add_argument('-i', '--db_host', type=str, help='数据库地址')
    parser.add_argument('-p', '--db_port', type=int, help='数据库端口')
    parser.add_argument('-u', '--db_user', type=str, help='数据库用户名')
    parser.add_argument('-s', '--db_password', type=str, help='数据库密码')
    parser.add_argument('-d', '--db_database', type=str, help='数据库名称')

    parser.add_argument('-I', '--api_host', type=str, help='生成的WebAPI运行所在的地址')
    parser.add_argument('-P', '--api_port', type=int, help='生成的WebAPI运行所在的端口')
    parser.add_argument('-U', '--api_path', type=str, help='生成的WebAPI的URL根路径')

    parser.add_argument('-r', '--tar_root_dir', type=str, help='目标代码存放根路径，默认为项目根目录下dat/dist/目录')
    parser.add_argument('-v', '--tar_ver', type=str, help='目标代码版本号，默认为1.0.0')

    args = parser.parse_args()

    if not args.tar_root_dir:
        root_dir = os.path.dirname(parent_dir)
        args.tar_root_dir = os.path.join(root_dir, "dat", "dist")

    # 创建参数字典，过滤掉未提供的参数
    task_info_params = {
        'db_host': args.db_host,
        'db_port': args.db_port,
        'db_user': args.db_user,
        'db_password': args.db_password,
        'db_database': args.db_database,
        'api_host': args.api_host,
        'api_port': args.api_port,
        'api_path': args.api_path,
        'tar_root_dir': args.tar_root_dir,
    }

    # 过滤掉值为 None 的参数
    filtered_params = {k: v for k, v in task_info_params.items() if v is not None}
    #print(filtered_params)

    # 调用 getTaskDesc，获取
    taskInfo = getTaskDesc(**filtered_params)
    print(f"\n{'#'*10} JYGT-CODER 准备处理下面任务：{'#'*10}\n{taskInfo}")

    # 初始化任务
    print(f"\n1. 加载数据库，并初始化目标工程目录{taskInfo.getTargetProjectInfo().getRootDir()}")
    taskInfo.init()
    print(f"\t << 加载表数量： {len(taskInfo.getDBInfo().getInfo())}")
    print(f"\t << 初始化目标工程目录 {taskInfo.getTargetProjectInfo().getRootDir()}")
    taskInfo.getTargetProjectInfo().dumpRootDir()
    #print(taskInfo.getTargetProjectInfo().getDirOfSrc())

    # 生成代码
    print(f"\n2. 生成代码")
    print(f"\t << 生成{ '成功' if generateCode(taskInfo) else '失败'}")

    print(f"\n{'#'*10} JYGT-CODER 任务处理完毕{'#'*10}\n")
if __name__ == "__main__":
    main()